/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

//#include "MyWebServer.h"

//Ethernet Chips
#ifdef __TM4C1294NCPDT__
#include <Ethernet.h>
#include <EthernetUdp.h>
#include "lwip/igmp.h"
#define NetUDP EthernetUDP
#define NetClient EthernetClient
#define Net Ethernet
#endif

//Wifi Chips
#ifdef __CC3200R1M1RGC__
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <SLFS.h>
#include <WiFiServer.h>
#define NetUDP WiFiUDP
#define NetClient WiFiClient
#define Net WiFi
#endif


#include "Multicast.h"


#include <stdarg.h>

#define MTU 1500

struct channel {
  WebServer server;
  char * variable;
  char * name;
  char * serial;
  uint8_t stat;
};


NetUDP multicast_listener;
#define MAX_CHANNELS 2
struct channel channels[MAX_CHANNELS];

volatile uint8_t num_channels = 0;
#define BASE_CHANNEL_PORT 12343

void setupChannel(struct channel *chan){
  chan->server.setDefaultCommand(&ssdpControlHTTPCmd);
  chan->server.setFailureCommand(&ssdpControlHTTPCmd);
  chan->server.addCommand("setup.xml",&ssdpSetupHTTPCmd);
  chan->server.begin();
}

int getMyChannelNumber(WebServer &server){
  for(int i=0;i<num_channels;i++){
    if(channels[i].stat > 0 && channels[i].server.port() == server.port()){
      return i;
    }
  }
  return -1;
}

int getMyUDPChannelNumber(int udpPort){
  for(int i=0;i<num_channels;i++){
    if(channels[i].stat > 0 && channels[i].server.port() == udpPort){
      return i;
    }
  }
  return -1;
}

void ssdpSetupHTTPCmd(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete){
  char replyBuffer[3000];
  memset(&replyBuffer,0,3000);

  int myChannelIndex = getMyChannelNumber(server);
  
  snprintf(replyBuffer,3000,
     "<?xml version=\"1.0\"?>\n<root>\n<device>\n<deviceType>urn:MoWe:device:lightswitch:2</deviceType>\n<friendlyName>%s</friendlyName>\n<manufacturer>Belkin International Inc.</manufacturer>\n<modelName>Emulated Socket</modelName>\n<modelNumber>Thingly100</modelNumber>\n<UDN>uuid:Socket-1_0-%s</UDN>\n</device>\n</root>\n",
     channels[myChannelIndex].name,
     channels[myChannelIndex].serial
     );
   Serial.println(replyBuffer);   

   server.httpSuccess();
   server.printP(replyBuffer); 
}

#define READ_BUFFER_SIZE 3000
void ssdpControlHTTPCmd(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete){
   char readBuffer[READ_BUFFER_SIZE+1];   //+1 in order to leave a null terminator
   memset(&readBuffer,0,READ_BUFFER_SIZE+1);
   int readBufferIndex = 0;
   
   
   
   int i = 0;
   while((i = server.read()) >= 0){
      readBuffer[readBufferIndex] = (char)i;
      readBufferIndex++;

      if(readBufferIndex == READ_BUFFER_SIZE){ 
        server.httpFail();
        server.printP("Control Command exceeded read buffer of ");
        server.print(READ_BUFFER_SIZE);
        server.printP(" bytes");
        Serial.print("Control Command exceeded read buffer of ");
        Serial.print(READ_BUFFER_SIZE);
        Serial.println(" bytes");
        return;
      }
   }

   char *binaryStatePtr = strstr(readBuffer,"<BinaryState>");
   if(binaryStatePtr == 0){
        server.httpFail();
        server.printP("Control Command did not include BinaryState XML element");
        Serial.println("Control Command did not include BinaryState XML element");
        return;
   }
   
   int myChannelIndex = getMyChannelNumber(server);

   if(binaryStatePtr[13] == '1'){
      my_free(put_stored_value(channels[myChannelIndex].variable, "true",stored_values));
   }else{
      my_free(put_stored_value(channels[myChannelIndex].variable, "false",stored_values));
   }

   
   server.httpSuccess();
}

int mewo_new_channel(char *variable,char *name,char *serial){
  uint8_t new_channel_num = num_channels;
  num_channels++;
  
  channels[new_channel_num].server = WebServer("",BASE_CHANNEL_PORT+new_channel_num);
  channels[new_channel_num].variable = variable;
  channels[new_channel_num].name = name;
  channels[new_channel_num].serial = serial;
  channels[new_channel_num].stat = 1;
  
  setupChannel(&channels[new_channel_num]);
}

void mewo_setup() {
  mewo_new_channel("bar_table","Bar Table","12344");
  mewo_new_channel("foo_table","Foo Table","12343");
  
  
  Serial.println(Net.localIP());
  multicast_listener.begin(1900);
  Serial.println("SSDP listening on 1900");

  Serial.println("Joining: ");

  //Join the multicast groups used for SSDP and WeMo device discovery
  Serial.print("239.255.255.250: ");
  Serial.println(mcast.join(239,255,255,250));
  Serial.print("239.011.003.008: ");
  Serial.println(mcast.join(239,11,3,8));
}



void mewo_loop() {
  char packetBuffer[MTU];

  for(int i=0;i<num_channels;i++){
    if(channels[i].stat > 0){
      channels[i].server.processConnection();
    }
  }
  
  // if there's data available, read a packet
  int packetSize = multicast_listener.parsePacket();
  if(packetSize) {
    // read the packet into packetBufffer
    memset(&packetBuffer,0,MTU);
    multicast_listener.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);


    if(strncmp(packetBuffer,"M-SEARCH ",8) == 0){
     //check if this is a belkin discovery packet
      char *searchTypeIndex = packetBuffer;
      char isBelkin = 0;
      while(true){
        if(strncmp(searchTypeIndex,"ST: urn:Belkin:device:**",24) == 0){
          //found it!
          
          isBelkin = 1;
          break;
        }
  
        
        while(searchTypeIndex[0] != '\0' && searchTypeIndex[0] != '\n'){
          searchTypeIndex++;
        }
        if(searchTypeIndex[0] == '\n'){
          searchTypeIndex++;
        }else{
          isBelkin = 0;
          break;
        }
      }
  
      if(isBelkin){
        Serial.println("BELKIN DISCOVERY FOUND");
        Serial.println("Contents:");
        Serial.println(packetBuffer);

        for(int i=0;i<num_channels;i++){
          sendSearchResponse(multicast_listener,i);
        }
        
        
        
      }
    }

  }
}


void sendSearchResponse(NetUDP udpSocket,int index){
  char replyBuffer[MTU];
  char *bufferIndex = replyBuffer;
  memset(&replyBuffer,0,MTU);

  //ip_addr_t ipaddr;
  //ip4_addr_set_u32(&ipaddr,lwIPLocalIPAddrGet());
  
  strcpy(bufferIndex,"HTTP/1.1 200 OK\nCACHE-CONTROL: max-age=86400\nDATE: ");
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  strcpy(bufferIndex,"Fri, 11 Nov 2016 08:12:31 GMT"); //TODO: Fill this in with the current date/time
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  strcpy(bufferIndex,"\nEXT:\nLOCATION: http://");
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  snprintf(bufferIndex,MTU,"%03d.%03d.%03d.%03d",Net.localIP()[0],Net.localIP()[1],Net.localIP()[2],Net.localIP()[3]);
  //ipaddr_ntoa_r(&ipaddr, bufferIndex,MTU);
  
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  strcpy(bufferIndex,":");
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  _ltostr(bufferIndex,channels[index].server.port());
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  strcpy(bufferIndex,"/setup.xml\nOPT: \"http://schemas.upnp.org/upnp/1/0\"; ns=01\n01-NLS: {55aefbe6-8310-4c44-82f3-1024e1032ad3}\nSERVER: 0.0.1\nST: urn:Belkin:device:**\n");
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  strcpy(bufferIndex,"USN: uuid:Socket-1_0-");
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  strcpy(bufferIndex,channels[index].serial);
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  strcpy(bufferIndex,"::urn:Belkin:device:**\n\n");
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  
  
  
  Serial.print("Length: ");
  Serial.println(((uint32_t)bufferIndex)-((uint32_t)(&replyBuffer)));
  Serial.write(replyBuffer);
  
  udpSocket.beginPacket(udpSocket.remoteIP(), udpSocket.remotePort());
  udpSocket.write(replyBuffer);
  udpSocket.endPacket();
}



