/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
//Ethernet Chips
#ifdef __TM4C1294NCPDT__
#include <Ethernet.h>
#include <EthernetUdp.h>
#include "lwip/igmp.h"
#define NetUDP EthernetUDP
#define NetClient EthernetClient
#endif

//Wifi Chips
#ifdef __CC3200R1M1RGC__
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <SLFS.h>
#include <WiFiServer.h>
#include <WiFi.h>
#define NetUDP WiFiUDP
#define NetClient WiFiClient
#endif



#include <stdarg.h>

#define MAX_UDP 1500

NetUDP multicast_datalogger;
#include "Multicast.h"

#define THINKULATOR_DATALOGGER_PORT 8472

void datalogger_setup (){

  Serial.println("Beginning datalogger.");
  Serial.println(multicast_datalogger.begin(8472));
  Serial.println("data logger listening on 8472");

  Serial.print("Logger joining: ");
  Serial.print("239.084.072.001: ");
  
  //Join the multicast groups used for Thinkulator data logging multicast
  Serial.println(mcast.join(239,84,72,1));

  
}

void datalogger_receive(){
  uint8_t *receiveBuffer = (uint8_t *) malloc(MAX_UDP);
  
  if ( multicast_datalogger.parsePacket() ) {
    Serial.println("Got Datalogger Packet");
    multicast_datalogger.read(receiveBuffer,MAX_UDP);
  }
  free(receiveBuffer);
}

void datalogger_transmit(struct value_store *tree) {

  
  char replyBuffer[MAX_UDP];
  char *bufferIndex = replyBuffer;
  memset(&replyBuffer,0,MAX_UDP);
  
  uint32_t sec = 0;
  uint16_t msec = 0;
  date.getTime(&sec,&msec);


  strcpy(bufferIndex,"{\"time\":\"");
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  formatTime(bufferIndex,128,sec,msec); 
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  
  snprintf(bufferIndex,1500,"\",\"id\":\"%s\",\"data\":{",macAddress);
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
  
  datalogger_transmit_value(tree,&bufferIndex);
  //trim the last comma ;)
  
  bufferIndex--;
  strcpy(bufferIndex,"}}");
  while(bufferIndex[0] != 0){ bufferIndex++; } //go to the end of the string
    
  multicast_datalogger.beginPacket(IPAddress(239,84,72,1), THINKULATOR_DATALOGGER_PORT);
  size_t toSend=strlen(replyBuffer);
  size_t sent=0;
  do{
    uint8_t *pt = (uint8_t *)&replyBuffer;
    pt+=sent;
    size_t r = multicast_datalogger.write(pt,toSend);
    sent+=r;
    toSend-=r;
  }while(toSend >0);
  
  multicast_datalogger.endPacket();

}

void datalogger_transmit_value(struct value_store *tree,char **bufferIndex){
    //output myself
    strcpy(*bufferIndex,"\"");
    while((*bufferIndex)[0] != 0){ (*bufferIndex)++; } //go to the end of the string
    strcpy(*bufferIndex,tree->name);
    while((*bufferIndex)[0] != 0){ (*bufferIndex)++; } //go to the end of the string
    strcpy(*bufferIndex,"\":\"");
    while((*bufferIndex)[0] != 0){ (*bufferIndex)++; } //go to the end of the string
    strcpy(*bufferIndex,tree->value);
    while((*bufferIndex)[0] != 0){ (*bufferIndex)++; } //go to the end of the string
    
    
    strcpy(*bufferIndex,"\",");
    while((*bufferIndex)[0] != 0){ (*bufferIndex)++; } //go to the end of the string
    

    if(tree->left != 0){
      datalogger_transmit_value(tree->left,bufferIndex);
    }
      
    if(tree->right != 0){
      datalogger_transmit_value(tree->right,bufferIndex);
    }
}


