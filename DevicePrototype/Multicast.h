/** Copyright 2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#ifndef Multicast_VERSION

#define Multicast_VERSION 001

class Multicast {
public:
    
    Multicast();

    int join(uint8_t fi,uint8_t se,uint8_t th,uint8_t fo);

    int leave(uint8_t fi,uint8_t se,uint8_t th,uint8_t fo);
    
private:

};

Multicast mcast;
#endif




