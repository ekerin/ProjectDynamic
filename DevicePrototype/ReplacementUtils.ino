/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
/*
void analogReadNoBlockInit(uint8_t pin,uint8_t adc) {
    uint8_t port = digitalPinToPort(pin);
    uint32_t channel = digitalPinToADCIn(pin);
    if (channel == NOT_ON_ADC) { //invalid ADC pin
        return;
    }
    ROM_SysCtlPeripheralEnable(adc?SYSCTL_PERIPH_ADC1:SYSCTL_PERIPH_ADC0);
    if(channel != ADC_CTL_TS)
        ROM_GPIOPinTypeADC((uint32_t) portBASERegister(port), digitalPinToBitMask(pin));
    ROM_ADCSequenceConfigure(adc?ADC1_BASE:ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
    ROM_ADCSequenceStepConfigure(adc?ADC1_BASE:ADC0_BASE, 3, 0, channel | ADC_CTL_IE | ADC_CTL_END);
    ROM_ADCSequenceEnable(adc?ADC1_BASE:ADC0_BASE, 3);

    ROM_ADCIntClear(adc?ADC1_BASE:ADC0_BASE, 3);
    ROM_ADCProcessorTrigger(adc?ADC1_BASE:ADC0_BASE, 3);
    
    
    return;
}


void analogReadNoBlockStart(uint8_t adc) {
    ROM_ADCIntClear(adc?ADC1_BASE:ADC0_BASE, 3);
    ROM_ADCProcessorTrigger(adc?ADC1_BASE:ADC0_BASE, 3);
    
    return;
}

uint8_t analogReadDone(uint8_t adc){
    return ROM_ADCIntStatus(adc?ADC1_BASE:ADC0_BASE, 3, false);
}


uint16_t analogReadNoBlock(uint8_t adc) {
    uint16_t value[1];
    
    //ensure the reading is ready, and if not - block till it is (yes, counterintuitive...)
    while(!ROM_ADCIntStatus(adc?ADC1_BASE:ADC0_BASE, 3, false)) {
    }

    
    ROM_ADCIntClear(adc?ADC1_BASE:ADC0_BASE, 3);
    ROM_ADCSequenceDataGet(adc?ADC1_BASE:ADC0_BASE, 3, (unsigned long*) value);

    return value[0];
}*/


