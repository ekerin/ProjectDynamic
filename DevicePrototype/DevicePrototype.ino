/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <stdarg.h>

#include "MyWebServer.h"
#include <flash.h>
#include "RTCDate.h"

#ifdef __CC3200R1M1RGC__
extern char *heap_end;
extern unsigned long _heap;
extern unsigned long _eheap;
extern unsigned long _bss;
#endif

#ifdef __TM4C1294NCPDT__
#include <eeprom.h>
#include <driverlib/hibernate.h>
#include <driverlib/sysctl.h>
#include "lwip/dhcp.h"

#include <Ethernet.h>
#include <EthernetUdp.h>
#define NetUDP EthernetUDP
#define NetClient EthernetClient
#define Net Ethernet

#define NetEther 1
#define LCD_DISPLAY 1

const long addr=0x40000L; //256KiB in
#define SLOT_SIZE 0x4000L 
#define ERASE_SIZE 0x4000L //any smaller, and the flash block size overlaps;
#define PAGES 48

extern unsigned long _etext;
extern unsigned long _data;
extern unsigned long _edata;
extern unsigned long _bss;
extern unsigned long _ebss;
extern char * heap_end;

#endif

//Wifi Chips
#ifdef __CC3200R1M1RGC__
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <SLFS.h>
#include <WiFiServer.h>
#include <WiFi.h>
#define NetUDP WiFiUDP
#define NetClient WiFiClient
#define Net WiFi

#define NetWiFi 1

const long addr=0x20030000L; //64KiB of 256KiB
#define SLOT_SIZE   0x1000L //flash block size is 4KiB
#define ERASE_SIZE  0x1000L
#define PAGES 16


#endif


#include "DynamicOperations.h"
#include "ValueTree.h"


#include <Servo.h> 


#ifdef LCD_DISPLAY
#include <LiquidCrystal.h>
LiquidCrystal lcd(PL_3, PL_2, PL_1, PL_0, PL_5, PL_4, PG_0, PF_3, PF_2, PF_1);
#define LCD_WIDTH_CHARS 16
#endif


//Includes for bc number library
#include "bcconfig.h"
#include "number.h"
#include <assert.h>
//End Includes for bc number library


RTCDate date;


int mainloop();
void notifier_connect();


const char input_pull[] = {PUSH1,PUSH2};
const char input_analog[] = {A0,A1,A2,A3};
const char servo[] = {};


const char output_down[] = {
#ifdef __TM4C1294NCPDT__
  #ifndef LCD_DISPLAY                      
                       PL_3,PL_2,PL_1,PL_0,PH_3, //Boosterpack 1 Pull Down
  #endif
                       PK_7,PK_6,PH_1,PH_0,PA_7 //Boosterpack 2 Pull Down
                      };
#endif
#ifdef __CC3200R1M1RGC__
                      {RED_LED};
#endif

const char output_up[] = {
#ifdef __TM4C1294NCPDT__
  #ifndef LCD_DISPLAY                      
                       PM_3,PF_1,PF_2,PF_3,PG_0, //Boosterpack 1 Pull Up
  #endif
                       PM_7,PG_1,PK_4,PK_5,PM_0 //Bootserpack 2 Pull Up
                      };
#endif
#ifdef __CC3200R1M1RGC__
                      {PIN_02};
#endif

#define NO_PAGE 0xFFFF


struct output_config {
  char * (*current_handler)(unsigned long now,struct operations_ptr *ptr);  //32 bits
  uint16_t current_page = NO_PAGE;  //8 bits
  uint32_t last_output_value=0;
};

#define OUTPUTS 20
struct output_config outputs[OUTPUTS];


#define PAGE_FREE      0x00
#define PAGE_TO_ERASE  0x01
#define PAGE_WRITING   0x02
#define PAGE_INUSE     0x03
#define PAGE_CONTINUE  0x04

uint8_t page_status[PAGES];


IPAddress server(192,168,1,111); 
NetClient client;

char macAddress[13];

unsigned long __current_running_block_addr = 0x00;
uint32_t *failreturn;
struct operations_ptr p;

struct hex_line{
  uint8_t cur_offset;
  uint8_t checksum_calc;

  uint8_t byte_count;
  uint16_t address;
  uint8_t record_type;
  uint8_t data[255];
  uint8_t checksum;
};

char * _ltostr (char *str,          /* output string */
    long val           /* value to be converted */)
{

    long q,r;

    if (val < 0){
      *str++ = '-';
      val=val*-1;
    }

    r = val%10;
    q = val/10;

    /* output digits of val/base first */

    if (q > 0)  str = _ltostr (str,q);

    /* output last digit */
    
    *str++ = '0'+r;
    *str   = '\0';
    return str;
}



uint8_t hexCharToInt(char next){
  if( (next >= 'A' && next <= 'F') ){
    return (next-'A'+10);
  }else if( (next >= 'a' && next <= 'f') ){
    return (next-'a'+10);
  }else if((next >= '0' && next <= '9') ){
    return (next-'0');
  }
  return 0;
}


/** Character by character parsing of a ihex line.  State is stored in hex_line st */
int update(const char next,struct hex_line *st){
  st->cur_offset++;

  if(next == '\n'){
    //end of line
    if(st->checksum_calc == 0){
       return 10;
    }else{
       return -10;
    }
  }else if(next == '\r'){
  
  }else if(next == ':'){
    //begin of line
    memset(st,0,sizeof(struct hex_line));
    st->cur_offset = 0;

  }else if( (next >= 'A' && next <= 'F') || (next >= '0' && next <= '9') ){
    uint8_t val = 0;

    val=hexCharToInt(next);

    if(st->cur_offset%2 == 1){
      val = val<<4;
    }
    st->checksum_calc+=val;


    if(st->cur_offset >= 1 && st->cur_offset <= 2){
      st->byte_count|=val;
    }else if(st->cur_offset >= 3 && st->cur_offset <= 4){
      st->address|= (val <<8);
    }else if(st->cur_offset >= 5 && st->cur_offset <= 6){
      st->address|= val;
    }else if(st->cur_offset == 7 || st->cur_offset == 8){
      st->record_type|= val;
    }else if(st->cur_offset >= 9 && st->cur_offset < 9+(st->byte_count*2)){
      st->data[(st->cur_offset-9)/2] |= val;
    }else if(st->cur_offset >= 9+(st->byte_count*2)){
      st->checksum |= val;
    }
  }
  return 0;
}





struct hex_line st;
uint16_t cur_page = NO_PAGE;


uint16_t getFreePage(){
   for(int i=0;i<PAGES;i++){
      if(page_status[i] == PAGE_FREE){
        return i;
      }
   }
}

uint16_t getExistingPage(const char *hash){
  uint8_t sha256_sig[32];
  memset(&sha256_sig,0,32);
  
  for(int i=0;i<64;i++){
    uint8_t val = hexCharToInt(hash[i]);
    if((i%2)==0){
      val = (val<<4);
    }
    sha256_sig[i/2] |=val;
  }
//  Serial.print("Passed: ");
//  Serial.println(hash);
//  Serial.print("Calc'd: ");
//  PrintHex8(sha256_sig, 32);
//  Serial.println();

  for(int i=0;i<PAGES;i++){
    if(page_status[i] == PAGE_INUSE){
      //does it have the same signature?
      
      block_header *pageHeader = (block_header *) (addr+(SLOT_SIZE*i));
      
      boolean found=1; //assume we found it, but we'll kill that if anything does not match
      for(int j=0;j<32;j++){
        if(pageHeader->sha256_sig[j] != sha256_sig[j]){
          found=0;
          break; //not it!
        }
      }
      if(found){
        return i;  
      }
    }
  }
  

  return NO_PAGE; //not found
}

void rebuildPageStatus(){
//REWRITE NEEDED
  //loop through the pages, and check if anyone still references this page
  //if no one references it, mark the page as TO_ERASE
  //erase the block once all the pages are marked TO_ERASE
/*  for(int i=0;i<PAGES;i++){
    //if the page is marked as in-use, check.
    if(page_status[i] == PAGE_INUSE){
      int found=0;
      for(int j=0;j<OUTPUTS;j++){
        if(outputs[j].current_page == i){
          found=1;
          page_status[outputs[j].current_page] = PAGE_INUSE;
          Serial.print("page ");
          Serial.print(outputs[j].current_page);
          Serial.print(" in use by output ");
          Serial.println(i);
          break;
        }
      }
      if(!found){
        //page is not in use anymore
        Serial.print("page ");
        Serial.print(i,DEC);
        Serial.println(" it no longer in use.");
        page_status[i] = PAGE_TO_ERASE;
        
        //check for any continuation pages
        while(page_status[i+i] == PAGE_CONTINUE){
          i++;
          page_status[i] = PAGE_TO_ERASE;
          Serial.print("page ");
          Serial.print(i,DEC);
          Serial.println(" it no longer in use.");
        }
      }
    }else if(page_status[i] == PAGE_TO_ERASE){
      Serial.print("page ");
      Serial.print(i,DEC);
      Serial.println(" marked for erase.");
    }else{
      Serial.print("page ");
      Serial.print(i,DEC);
      Serial.print(" marked ");
      Serial.println(page_status[i]);
      
    }

  }
  */
  //clean out all page status, and then rebuild from output references
  memset(&page_status,0,PAGES);
  
  for(int i=0;i<OUTPUTS;i++){
    if(outputs[i].current_page != NO_PAGE){
      page_status[outputs[i].current_page] = PAGE_INUSE;
      Serial.print("page ");
      Serial.print(outputs[i].current_page);
      Serial.print(" in use by output ");
      Serial.println(i);
    }
  }
  
}

void storeOutputPages(){
  uint16_t storedOutputPages[((OUTPUTS/4)+1)*4]; //must be at least OUTPUTS in size, plus word alignment.
  memset(&storedOutputPages,0xFF,sizeof(storedOutputPages));
  
  
  //Convert the output pages to the stored format.
  for(int i=0;i<OUTPUTS;i++){
    storedOutputPages[i] = outputs[i].current_page;
    Serial.println(outputs[i].current_page);
  }
  //EEPROMProgram((uint32_t *)&storedOutputPages, 0, sizeof(storedOutputPages));
}



int updating_output=NO_PAGE;
int data_started =0;
static uint8_t new_code_buffer[4096];

void http_req(int num, const char *hash){
  
  updating_output = num;
  data_started=0;
  //failed writes leave stagnant pages behind, cleanup.
  rebuildPageStatus();
  cur_page = getExistingPage(hash);
  if(cur_page != NO_PAGE){
    Serial.print("Re-using code at page: ");
    Serial.print(cur_page);
    Serial.print(" for output ");
    Serial.println(num);
    block_header *newBlockHeader = (block_header *) (addr+(SLOT_SIZE*cur_page));
    
    outputs[num].current_page = cur_page;
    outputs[num].current_handler = (char * (*)(unsigned long now, struct operations_ptr *ptr))((addr+(SLOT_SIZE*cur_page)+newBlockHeader->entry_offset));
    
    return;
  }

  memset(&new_code_buffer,0,sizeof(new_code_buffer));
  
  cur_page = getFreePage();
  
  
 
// TODO: Move flash erase into rebuild, only when the right alignment of pages are ready (since page size and flash erase size are not the same
  Serial.print("Begin Flash Erase for page ");
  Serial.print(cur_page,HEX);
  Serial.print(" at ");
  Serial.print(addr+(SLOT_SIZE*cur_page));
  Serial.print(" for ");
  Serial.println(hash);

  
  #ifdef __TM4C1294NCPDT__
    Serial.println(FlashErase(addr+(SLOT_SIZE*cur_page)));
  #endif

  #ifdef __CC3200R1M1RGC__
    #warning Flash Erase Disabled  
  #endif

  


  data_started =0;
 
  if (client.connect(server, 8084)) {
    Serial.println("connected");
    // Make a HTTP request:

    client.print("GET /DynamicWeb/Formula/?binhash=");
    client.print(hash);
    //client.print("&channel=");
    //client.print(num);
    client.println(" HTTP/1.1");
    client.println("host: localhost");
    client.println("Connection: close");

    client.println();
  }else{
    // kf you didn't get a connection to the server:
    Serial.println("connection failed");
  }
}


int charsSinceLastNewline = 999999;


void http_loop(){
 //Serial.println((uint32_t)client,HEX);
 if (client.connected() && client.available()) {
    char inChar = client.read();
   
    if(data_started == 1){
       int ret = update(inChar,&st);
       if(ret <0){
         Serial.println("ERROR, Bad data");
       }else if(ret == 10){
          if(st.record_type == 0x00){
            //TI Launchpad requires flash writes to be 32 bits wide.
            int to_write = st.byte_count;
            if(to_write%4 != 0){
              to_write+= 4-(to_write%4);
            }

            page_status[cur_page] = PAGE_WRITING;
          

            
            #ifdef __TM4C1294NCPDT__
              Serial.print("Flash Write ");
              Serial.println(FlashProgram((uint32_t *)&st.data,addr+(SLOT_SIZE*cur_page)+st.address,to_write));
            #endif
        
            #ifdef __CC3200R1M1RGC__
              Serial.print("Memory Write ");
              Serial.print(addr+(SLOT_SIZE*cur_page)+st.address,HEX);
              Serial.print("...");
              memcpy((void *)(addr+(SLOT_SIZE*cur_page)+st.address),(uint32_t *)&st.data,to_write);
              Serial.println("Done");
            #endif
  

          }else if(st.record_type == 0x01){
            uint8_t selector = updating_output;
            uint8_t codeGood = 0;

            
           //Test the new code, if it crashes, we never come back to switch.
            __current_running_block_addr = addr+(SLOT_SIZE*cur_page);
            block_header *newBlockHeader = (block_header *) __current_running_block_addr;
            
            ((char * (*)(unsigned long now, struct operations_ptr *ptr))((addr+(SLOT_SIZE*cur_page)+newBlockHeader->entry_offset)))(0,&p);

          
         
            //Everything is good, store the new output config.          
            Serial.print("Switching output ");
            Serial.print(selector);
            Serial.print(" to page ");
            Serial.print(cur_page);
            Serial.print(" at ");
            Serial.println(addr+(SLOT_SIZE*cur_page));

            page_status[cur_page] = PAGE_INUSE;

            outputs[selector].current_page = cur_page;
            outputs[selector].current_handler = (char * (*)(unsigned long now, struct operations_ptr *ptr))((addr+(SLOT_SIZE*cur_page)+newBlockHeader->entry_offset));

  
            rebuildPageStatus();

            //update the EEPROM stored config.
            storeOutputPages();
            Serial.println("Active");
          }
       }
    }else{
      if(charsSinceLastNewline == 0 && inChar =='\n'){
        Serial.println("----DATA START----");
        data_started=1;
      }else{
        Serial.print(inChar);
      }
    
      if(inChar == '\n'){
         charsSinceLastNewline = 0;
      }else if(inChar == '\r'){
        //ignore;
        return;
      }else{
        charsSinceLastNewline++;
      }
    }
    
  }

  // if the server's disconnected, stop the client:
  if (client != 0 && !client.connected()) {
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();
  }
  
}
NetClient notifier;

struct notification {
  char channel[32];
  char hash[65];
};

int notifier_read_state = 0; //0 = no message, 1 = reading channel, 2=reading hash
int position = 0;

struct notification current_notify;


void notifier_loop(){

  //make sure the connection remains open (reopen it if it's closed)
  notifier_connect();
  
  //if the notifier has data available, and we're not already downloading data via HTTP
  if (notifier.available()  && !client.connected()) {
    char inChar = notifier.read();
    if(notifier_read_state==0){
      //starting a message
      notifier_read_state = 1;
      position=0;
    }

    if(notifier_read_state == 1){
      if(inChar == ','){
        current_notify.channel[position] = 0;
        position=0;
        notifier_read_state = 2;
      }else{
        current_notify.channel[position] = inChar;
        position++;
      }
    }else if(notifier_read_state == 2){
      if(inChar == '\n'){
        //complete!
        current_notify.hash[position] = 0; //null terminator
        http_req(atoi(current_notify.channel),current_notify.hash);
        notifier_read_state = 0;
      }else{
        current_notify.hash[position] = inChar;
        position++;
      }
    }
    
  }
}

//-------BEGIN FUNCTION UTILS ------





struct value_store *stored_values;




static char * __attribute__ ((noinline)) calc_noop(unsigned long now,struct operations_ptr *ptr){
   return "false";
}


//Platform specific utilities


extern "C"
{
  int fault_handler(uint32_t stack,uint32_t isr);
}

__attribute__((used))
int fault_handler(uint32_t stack,uint32_t isr){
  stack+=8; //the caller stacks 2 entries because of our two parameters...

  
  Serial.print("FAULT CODE: ");
  Serial.println(HWREG(0xE000ED28L),HEX);

  Serial.print("FAULT ADDR: ");
  Serial.println(HWREG(0xE000ED38L),HEX);


  Serial.print("Current block: ");
  Serial.println(__current_running_block_addr,HEX);

  
  uint32_t * stackptr = (uint32_t *)(stack);
   
  Serial.print("Stack ADDR: ");
  Serial.println(stack,HEX);


  Serial.print("BSS ADDR: ");
  Serial.println(((uint32_t)&_bss),HEX);

#ifdef __CC3200R1M1RGC__

  Serial.print("Heap Start ADDR: ");
  Serial.println(((uint32_t)&_heap),HEX);

  Serial.print("Heap End ADDR: ");
  Serial.println(((uint32_t)&_eheap),HEX);
#elif defined(__TM4C1294NCPDT__)
  extern char end asm ("end");
  Serial.print("Heap Start ADDR: ");
  Serial.println(((uint32_t)&end),HEX);

  Serial.print("Heap End ADDR: ");
  Serial.println(((uint32_t)&heap_end),HEX);
#endif


  Serial.print("ISR Num: ");
  Serial.println(isr,HEX);

  Serial.println("Stack Dump: ");
  
  Serial.print("R0   :");Serial.println(HWREG(stackptr+0x00),HEX);
  Serial.print("R1   :");Serial.println(HWREG(stackptr+0x01),HEX);
  Serial.print("R2   :");Serial.println(HWREG(stackptr+0x02),HEX);
  Serial.print("R3   :");Serial.println(HWREG(stackptr+0x03),HEX);
  Serial.print("R12  :");Serial.println(HWREG(stackptr+0x04),HEX);
  Serial.print("LR   :");Serial.println(HWREG(stackptr+0x05),HEX);
  Serial.print("PC   :");Serial.println(HWREG(stackptr+0x06),HEX);
  Serial.print("xPSR :");Serial.println(HWREG(stackptr+0x07),HEX);

//TODO: Tell if the Floating Point unit is enabled, and therefore it's registers have been stacked.
  /*Serial.print("S0   :");Serial.println(HWREG(stackptr+0x08),HEX);
  Serial.print("S1   :");Serial.println(HWREG(stackptr+0x09),HEX);
  Serial.print("S2   :");Serial.println(HWREG(stackptr+0x0A),HEX);
  Serial.print("S3   :");Serial.println(HWREG(stackptr+0x0B),HEX);
  Serial.print("S4   :");Serial.println(HWREG(stackptr+0x0C),HEX);
  Serial.print("S5   :");Serial.println(HWREG(stackptr+0x0D),HEX);
  Serial.print("S6   :");Serial.println(HWREG(stackptr+0x0E),HEX);
  Serial.print("S7   :");Serial.println(HWREG(stackptr+0x0F),HEX);
  Serial.print("S8   :");Serial.println(HWREG(stackptr+0x10),HEX);
  Serial.print("S9   :");Serial.println(HWREG(stackptr+0x11),HEX);
  Serial.print("S10  :");Serial.println(HWREG(stackptr+0x12),HEX);
  Serial.print("S11  :");Serial.println(HWREG(stackptr+0x13),HEX);
  Serial.print("S12  :");Serial.println(HWREG(stackptr+0x14),HEX);
  Serial.print("S13  :");Serial.println(HWREG(stackptr+0x15),HEX);
  Serial.print("S14  :");Serial.println(HWREG(stackptr+0x16),HEX);
  Serial.print("S15  :");Serial.println(HWREG(stackptr+0x17),HEX);
  Serial.print("FPSCR:");Serial.println(HWREG(stackptr+0x18),HEX);
  Serial.print("     :");Serial.println(HWREG(stackptr+0x19),HEX);
  Serial.print("ali  :");Serial.println(HWREG(stackptr+0x20),HEX);*/
  
  if(__current_running_block_addr == 0){
    return 1;
  }else{
    for(int i=0;i<OUTPUTS;i++){
      //Serial.print("Checking :");
      //Serial.println((addr+(SLOT_SIZE*outputs[i].current_page)),HEX);
       if(__current_running_block_addr == (addr+(SLOT_SIZE*outputs[i].current_page))){
         Serial.print("Resetting faulty handler for output ");
         Serial.println(i);
         outputs[i].current_handler = (char * (*)(unsigned long now, struct operations_ptr *ptr))((&calc_noop));
       }
    }

    Serial.print("Fail Return: ");
    Serial.println((uint32_t)failreturn,HEX);
  
    HWREG(stackptr+0x06) = (uint32_t) failreturn;  //set new entry address
    HWREG(stackptr+0x07) = (uint32_t) 0x1000000L;   //reset the Program Status Register (specifically set the thumb intruction bit and clear any If-Then Status, a null pointer jump would clear it)
    
//clear the faults bit
    HWREG(0xE000ED28L) = HWREG(0xE000ED28L);
    HWREG(0xE000ED2CL) = HWREG(0xE000ED2CL);
    HWREG(0xE000ED30L) = HWREG(0xE000ED30L);
    HWREG(0xE000ED3cL) = HWREG(0xE000ED3CL);
    HWREG(0xE000ED34L) = HWREG(0xE000ED34L);
    HWREG(0xE000ED38L) = HWREG(0xE000ED38L);
    
    return 0;
  }
}


char * fix_pointer(char *ptr){
  char * res = &(ptr[__current_running_block_addr]);
  return res;
}


void my_free(void *ptr){
  //Don't try and free addresses in the program memory!


  //is the address range is in RAM
  #ifdef __TM4C1294NCPDT__
    if(ptr != 0 && ((uint32_t)ptr) > 0x20000000 ){
      free(ptr);
    }
  #endif  
  #ifdef __CC3200R1M1RGC__
    if(ptr != 0 && ((uint32_t)ptr) > ((uint32_t)&_heap) && ((uint32_t)ptr) < ((uint32_t)&_eheap) ){
      free(ptr);
    }
  #endif
}
//end platform specific utilities



unsigned long loopspeed_start;
unsigned long loopspeed_count;




void PrintHex8(uint8_t *data, uint8_t length){ // prints 8-bit data in hex with leading zeroes

       Serial.print("0x"); 
       for (int i=0; i<length; i++) { 
         if (data[i]<0x10) {
            Serial.print("0");
         } 
         Serial.print(data[i],HEX);  
       }
}

Servo myservo;
#define NUM_INPUT_CHANNELS 4

uint32_t frequency_last[NUM_INPUT_CHANNELS];
uint32_t input_counter_last[NUM_INPUT_CHANNELS];
uint32_t input_counter[NUM_INPUT_CHANNELS];
uint32_t input_last[NUM_INPUT_CHANNELS];
uint32_t input_threshold[NUM_INPUT_CHANNELS];


void _fini(void){}

void setup() {
  
  
  setup_operations_ptr(&p);
  
  bc_init_numbers();
  
  stored_values = new_value_store("duty","250");
  
  //Initialize serial and wait for port to open:
  Serial.begin(115200); 
  
  
#ifdef NetEther
  uint32_t ui32User0, ui32User1;
  
  ROM_FlashUserGet(&ui32User0, &ui32User1);
  snprintf(macAddress,13,"%02X%02X%02X%02X%02X%02X",
          (ui32User0 & 0xFF),
          ((ui32User0 >>8 )& 0xFF),
          ((ui32User0 >>16 )& 0xFF),
          (ui32User1 & 0xFF),
          ((ui32User1 >>8 )& 0xFF),
          ((ui32User1 >>16 )& 0xFF));


  Serial.print("Starting Ethernet for MAC: ");
  Serial.print(macAddress);
  Serial.println("...");

  
  //fire off DHCP and bring up ethernet, but do not wait for the IP address to be set.  We'll deal with that later.
  Ethernet.begin(0, IPAddress(0,0,0,0), IPAddress(0,0,0,0), IPAddress(0,0,0,0), IPAddress(0,0,0,0));
  
  //Ethernet.enableLinkLed();
  //Ethernet.enableActivityLed();
#elif defined(NetWiFi)
  uint8_t macRaw[6];
  WiFi.macAddress(macRaw);
  snprintf(macAddress,13,"%02X%02X%02X%02X%02X%02X",macRaw[0],macRaw[1],macRaw[2],macRaw[3],macRaw[4],macRaw[5]);
          
Serial.print("Starting WiFi for MAC: ");
  Serial.print(macAddress);
  Serial.println("...");

  WiFi.begin("bleep","stayaway");
#else 
 #error No Network
#endif

  
  
  //read the output config from EEPROM
  uint16_t storedOutputPages[((OUTPUTS/4)+1)*4]; //must be at least OUTPUTS in size, plus word alignment.
  memset(&storedOutputPages,0xFF,sizeof(storedOutputPages));

  
//  EEPROMRead((uint32_t *)&storedOutputPages, 0, sizeof(storedOutputPages));

  //Rebuild the output pages
  for(int i=0;i<OUTPUTS;i++){
    if(storedOutputPages[i] != NO_PAGE){
      //get the header for the block, so we can calculate entry points and types (as well we check that it is still there)
      block_header *newBlockHeader = (block_header *)(addr+(SLOT_SIZE*(storedOutputPages[i])));
      if(newBlockHeader->block_type != 0xFF){
        Serial.print("Enabling Output ");
        Serial.print(i);
        Serial.print(" to page ");
        Serial.print(storedOutputPages[i]);
        Serial.print(" at ");
        Serial.print(addr+(SLOT_SIZE*storedOutputPages[i]),HEX);
        Serial.print(" signature: ");
        PrintHex8(newBlockHeader->sha256_sig, 32);
        Serial.println();
        
        //calc the entry point based upon the entry point offset stored in the header block 
        outputs[i].current_handler = (char * (*)(unsigned long now, struct operations_ptr *ptr))((addr+(SLOT_SIZE*(storedOutputPages[i]))+newBlockHeader->entry_offset));
        outputs[i].current_page = storedOutputPages[i];
        continue; //we're good, next index
      }
      //if we got here, the program stored at this block is missing (possibly a firmware update?)
    }
    
    Serial.print("Enabling Output ");
    Serial.print(i);
    Serial.println(" to default handler");
      
    outputs[i].current_handler = (char * (*)(unsigned long now, struct operations_ptr *ptr))((&calc_noop));
    outputs[i].current_page = NO_PAGE;

#ifdef LCD_DISPLAY
    lcd.begin(LCD_WIDTH_CHARS, 2);
#endif    
  }
  
  rebuildPageStatus();



  //digitial input channels
  for(int i=0;i<sizeof(input_pull);i++){
    pinMode(input_pull[i], INPUT_PULLUP); 
  }

  //analog input channels
  for(int i=0;i<sizeof(input_analog);i++){
    pinMode(input_analog[i], INPUT); 
  }

  for(int i=0;i<sizeof(servo);i++){
    myservo.attach(servo[i]);
  }

  for(int i=0;i<sizeof(output_down);i++){
      pinMode(output_down[i], OUTPUT);
      digitalWrite(output_down[i],0);
  }
  for(int i=0;i<sizeof(output_up);i++){
      pinMode(output_up[i], OUTPUT);
      digitalWrite(output_up[i],0);
  }




  // currently only implemented for the analog input channels
  for(int i=0;i<NUM_INPUT_CHANNELS;i++){
    frequency_last[i]=0;
    input_counter_last[i]=0;
    input_counter[i]=0;
    input_threshold[i]=2048;
    input_last[i]=0; //TODO, pull from the real input channel
  }
  
  

  memset(&page_status,0,PAGES);


  loopspeed_count=0;
  loopspeed_start=millis();

  failreturn = (uint32_t *) (&mainloop);

  Serial.println("Startup Complete");
}

int lastAW = 0;

#define ANALOG_AVG_SAMPLES 50
uint32_t analog_avg[4][ANALOG_AVG_SAMPLES];

/** Returns the average of the last few readings for that channel */
uint32_t analog_averaging(uint8_t channel,uint16_t value){
  uint32_t sum = value;
  analog_avg[channel][ANALOG_AVG_SAMPLES-1] = value;
  //for(int i=0; i<value;i++){
  //  Serial.print(" ");
  //}
  //Serial.println("|");
    

  //shift everything back one, while adding them to the sum;
  for(int i=1;i< (ANALOG_AVG_SAMPLES);i++){
    analog_avg[channel][i-1] = analog_avg[channel][i];
    sum+=analog_avg[channel][i];
  }
  return sum/ANALOG_AVG_SAMPLES;
  
}

void execute_output(int num,int upPin,int downPin, long now){
  __current_running_block_addr = addr+(SLOT_SIZE*outputs[num].current_page);
  char *re =   outputs[num].current_handler(now,&p);
  int value = 0;
  if(isNumber(re,0)){
    value = atof(re)*255; //close enough... And WAY faster.
  }else{
    if(isTrue(re,0)){
      value = 255;
    }else{
      value = 0;
    }
  }
  
  if(outputs[num].last_output_value != value){
    outputs[num].last_output_value = value;
//    Serial.print("Changing Output ");
//    Serial.print(num);
//    Serial.print(" value to: ");
//    Serial.println(outputs[num].last_output_value);

    if(value < 0){
      digitalWrite(upPin,0);
      digitalWrite(downPin,1);
    }else{
      digitalWrite(downPin,0);
      analogWrite(upPin, outputs[num].last_output_value);  
    }
  }
  my_free(re);
}

void execute_servo_output(int num,int pin,long now){
  __current_running_block_addr = addr+(SLOT_SIZE*outputs[num].current_page);
  char *re =   outputs[num].current_handler(now,&p);
  int value = 0;
  if(isNumber(re,0)){
    re = do_mul(re,"180");
    value = atoi(re);
    value = (int)value/3.14159265358979f; //convert to radians
    value+=90;
  }else{
    if(isTrue(re,0)){
      value = 180;
    }else{
      value = -180;
    }
  }
  if(outputs[num].last_output_value != value){
    outputs[num].last_output_value = value;
    myservo.write(value);
    //Serial.print("Changing Output ");
    //Serial.print(num);
    //Serial.print(" value to: ");
    //Serial.println(outputs[num].last_output_value);
  }
  my_free(re);
}


void execute_variable_output(int num,char *name,long now){
  __current_running_block_addr = addr+(SLOT_SIZE*outputs[num].current_page);
  char *re =   outputs[num].current_handler(now,&p);
  my_free(put_stored_value(name, re,stored_values));
  
}

long last_notifier_connect_time = 0L;
int network_state = 0;

void notifier_connect(){
  //only try to connect we have not tried in the last 15 seconds
  if(network_state != 0 && !notifier.connected() && (last_notifier_connect_time+15000) < millis()){
    Serial.println("Connecting...");
    last_notifier_connect_time=millis();

    notifier_read_state = 0; //0 = no message
    position = 0;
    
    if (notifier.connect(server, 5555)) {
      Serial.println("Notifier Connected");
      char message[1500];
      snprintf(message,1500,"{\"address\":\"%s\"}",macAddress);
      notifier.print(message);
    }else{
      Serial.println("Notifier connection failed");
    }
    
  }
}

NetUDP ntpUdp;

void network_services_setup(){
  Serial.print("DHCP Complete.  IP Address: ");
  Serial.println(Net.localIP());

  Serial.println("Connecting to Notifier Service... ");
  notifier_connect();
  
  date.begin(&ntpUdp);
  
  mewo_setup();
  datalogger_setup();
  
}

char * scale_adc_reading(uint16_t reading){
    uint64_t scaled = 24420L*reading; 
    
    char * readingStr = (char *)malloc(16);
    memset(readingStr,'0',16);
    readingStr[16] = 0; //null terminator;
     
    readingStr[1] = '.';
    
    if(scaled == 99999900){
      readingStr[0] = '1';
    }else{
      for(int i=9;i>=2;i--){
        uint8_t thisCharVal = scaled%10;
        scaled = scaled/10;
        readingStr[i] = thisCharVal+'0';
      }
    }
    return readingStr;
}


void do_reading(uint16_t channel){
    int channel_ord = -1;
    switch(channel){
      case A0:
        channel_ord = 0; break;
      case A1:
        channel_ord = 1; break;
      case A2:
        channel_ord = 2; break;
      case A3:
        channel_ord = 3; break;
    }
    if(channel_ord == -1){
      Serial.print("Nonexisting Input Channel: ");
      Serial.println(channel);
      return;
    }

    
    uint32_t reading = analogRead(channel);
    //implement a schmidtt trigger counter for the falling edge
    if(reading > input_threshold[channel_ord]+50 && input_last[channel_ord] == 0){
      input_counter[channel_ord]++;
      input_last[channel_ord]=1;
    }else if(reading <input_threshold[channel_ord]-50 && input_last[channel_ord] ==1){
      input_last[channel_ord]=0;
    }
    
    char * readingStr = scale_adc_reading(reading);
    char * valueName = "";
    char * counterName = "";
    char * frequencyName = "";
    switch (channel_ord){
      case 0:
        valueName="analog0";counterName="analog0_counter";frequencyName="analog0_frequency";break;
      case 1:
        valueName="analog1";counterName="analog1_counter";frequencyName="analog1_frequency";break;
      case 2:
        valueName="analog2";counterName="analog2_counter";frequencyName="analog2_frequency";break;
      case 3:
        valueName="analog3";counterName="analog3_counter";frequencyName="analog3_frequency";break;
    }

    uint32_t now = millis();
    if(frequency_last[channel_ord]+2000 < now){
      uint32_t span = now-frequency_last[channel_ord];
      uint32_t count = input_counter[channel_ord] - input_counter_last[channel_ord];
      
      char *v = do_div(ltostr(count),do_div(ltostr(span),"1000"));
      v = do_add(v,"0.05");
      for(int i=0;v[i] !=0;i++){
        if(v[i] == '.'){
          v[i+2]='\0';  
        }
      }
      
      my_free(put_stored_value(frequencyName,v,stored_values));
      
      input_counter_last[channel_ord] = input_counter[channel_ord];
      frequency_last[channel_ord] = now;
    }
    
    my_free(put_stored_value(valueName, readingStr,stored_values));
    my_free(put_stored_value(counterName, ltostr(input_counter[channel_ord]),stored_values));
}

void loop() { 
  date.checkup();
  mewo_loop();


  unsigned long now;
  uint16_t nowmsec;
  
  date.getTime(&now,&nowmsec);
  
  if(network_state == 0){ //not yet started.
    if(Net.localIP() != 0){
      network_state = 1; //started
      network_services_setup();
    }
  }
  
  loopspeed_count++;
  char * reading_str;
  int reading;
  uint32_t value;

  for(int i=0;i<sizeof(input_analog);i++){
    do_reading(input_analog[i]);
  }

  reading = !digitalRead(PUSH1);
  reading_str = ltostr(reading);
  my_free(put_stored_value("push1", reading_str,stored_values));

  reading = !digitalRead(PUSH2);
  reading_str = ltostr(reading);
  my_free(put_stored_value("push2", reading_str,stored_values));


  execute_variable_output(10,"alpha",now);
  execute_variable_output(11,"beta",now);
  execute_variable_output(12,"charle",now);
  execute_variable_output(13,"delta",now);


#ifdef LCD_DISPLAY
  execute_variable_output(14,"lcd1",now);
  execute_variable_output(15,"lcd2",now);
#endif



  for(int i=0;i<sizeof(output_down);i++){
    execute_output(i,output_down[i],output_up[i],now);
  }
  
 

  __current_running_block_addr=0;
  
  http_loop();
  notifier_loop();
  datalogger_receive();
  
  if(network_state != 0 && loopspeed_count%500 == 0){
    datalogger_transmit(stored_values);
  }

#ifdef LCD_DISPLAY
  if(loopspeed_count%500 == 0){
    char *val = get_stored_value("lcd1", stored_values);
    if(val != 0){
      lcd.setCursor(0, 0);
      int l = strlen(val);
      
      lcd.print(val);
      //overwrite anything after the end of the string.
      for(;l<LCD_WIDTH_CHARS;l++){
        lcd.print(" ");
      }
    }
    lcd.setCursor(0, 1);
    
    val = get_stored_value("lcd2", stored_values);
    if(val != 0){
      lcd.setCursor(0, 1);
      int l = strlen(val);
      
      // print the number of seconds since reset:
      lcd.print(val);
      //overwrite anything after the end of the string.
      for(;l<LCD_WIDTH_CHARS;l++){
        lcd.print(" ");
      }
    }
    
  }
#endif  
  if(loopspeed_count == 1000){
   
    char buffer[128];
    
    memset(&buffer,0,128);
    
    //RTC
    uint32_t sec = 0;
    uint16_t msec = 0;
    date.getTime(&sec,&msec);
    formatTime(&buffer[0],128,sec,msec);   //HIBRTCC, //HIBRTCSS & mask off lower 16 bits;   ((HWREG( 0x400FC028L) & 0xFFFFL)*1000)/32768

  
    uint32_t now = millis();
    Serial.print(buffer);
    Serial.print("  Loop Speed - loops:");
    Serial.print(loopspeed_count);
    Serial.print(" TTime (ms):");
    Serial.print(now-loopspeed_start);
    Serial.print(" LTime (ms):");
    Serial.println((now-loopspeed_start)/(float)loopspeed_count);
    loopspeed_count=0;
    loopspeed_start=now;

#ifdef __CC3200R1M1RGC__

    Serial.print("Memory Stats ->  allocated (bytes): ");
    Serial.print((uint32_t)heap_end-((uint32_t)&_heap),DEC);

    Serial.print("  free (bytes): ");
    Serial.println(((uint32_t)&_eheap)-(uint32_t)heap_end,DEC);
#endif    
   
  }
  
}



