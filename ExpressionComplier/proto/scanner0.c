#include "utils.h"
//=AND(MOD(now,4000)>0,MOD(now,4000)<1000)


char  * __attribute__ ((noinline)) __attribute__((section(".entry"))) _start(unsigned long now,struct operations_ptr *ptr){
        return ptr->do_and(
		ptr->do_compare(
			ptr->do_mod(
				ptr->ltostr(now),
				ptr->ltostr(4000)
			),
			ptr->ltostr(0),
			COMPARE_GT
		),
		ptr->do_compare(
			ptr->do_mod(
				ptr->ltostr(now),
				ptr->ltostr(4000)
			),
			ptr->ltostr(1000),
			COMPARE_LT
		)
	);

}


