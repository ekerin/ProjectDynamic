#include "utils.h"


char  * __attribute__((noinline)) __attribute__((section(".entry"))) _start(unsigned long now,struct operations_ptr *ptr){
       return ptr->do_and(
		ptr->do_compare(
			ptr->do_mod(
				ptr->ltostr(now),
				ptr->FP("4000")
			),
			ptr->FP("3000"),
			COMPARE_GT
		),
		ptr->do_compare(
			ptr->do_mod(
				ptr->ltostr(now),
				ptr->FP("4000")
			),
			ptr->FP("4000"),
			COMPARE_LT
		)
	);

}


