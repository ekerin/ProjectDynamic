#include <stdint.h>
#include "number.h"
#include <stdlib.h>



struct bc_ptr {
	void (*bc_init_numbers)(void); 
	void (*bc_free_numbers)(void);
	bc_num (*bc_new_num)(int length, int scale);
	void (*bc_free_num)(bc_num *num);

	bc_num (*bc_copy_num)(bc_num num);

	void (*bc_init_num)(bc_num *num);

	void (*bc_str2num)(bc_num *num, const char *str, int scale);

	char *(*bc_num2str)(bc_num num);

	void (*bc_int2num)(bc_num *num, int val);

	long (*bc_num2long)(bc_num num);

	int (*bc_compare)(bc_num n1, bc_num n2);

	char (*bc_is_zero)(bc_num num);

	char (*bc_is_near_zero)(bc_num num, int scale);

	char (*bc_is_neg)(bc_num num);

	void (*bc_add)(bc_num n1, bc_num n2, bc_num *result, int scale_min);

	void (*bc_sub)(bc_num n1, bc_num n2, bc_num *result, int scale_min);

	void (*bc_multiply)(bc_num n1, bc_num n2, bc_num *prod, int scale);

	int (*bc_divide)(bc_num n1, bc_num n2, bc_num *quot, int scale);

	int (*bc_modulo)(bc_num num1, bc_num num2, bc_num *result,
			   int scale);

	int (*bc_divmod)(bc_num num1, bc_num num2, bc_num *quot,
				   bc_num *rem, int scale);

	int (*bc_raisemod)(bc_num base, bc_num expo, bc_num mod,
				     bc_num *result, int scale);

	void (*bc_raise)(bc_num num1, bc_num num2, bc_num *result,
				   int scale);

	int (*bc_sqrt)(bc_num *num, int scale);

	void (*bc_out_num) (bc_num num, int o_base, void (* out_char)(int),
				     int leading_zero);

  void * (*malloc)(size_t size);
  void   (*free)(void *ptr);


};

#define COMPARE_LT   1
#define COMPARE_GT   2
#define COMPARE_E    4
#define COMPARE_LTE  5 //1 or 4
#define COMPARE_GTE  6 //2 or 4

static char * do_mod(char *left, char *right,struct bc_ptr *ptr);
static char * do_compare(char *left, char *right, struct bc_ptr *ptr,char type);

static char *                  /* addr of terminating null */
ltostr (
    char *str,          /* output string */
    long val           /* value to be converted */
    )
{

    //if the string pointer has not been passed, allocate a buffer.
    long q,r;

    if (val < 0)    *str++ = '-';

    r = val%10;
    q = val/10;

    /* output digits of val/base first */

    if (q > 0)  str = ltostr (str,q);

    /* output last digit */
    
    *str++ = '0'+r;
    *str   = '\0';
    return str;
}


char  * __attribute__ ((noinline)) __attribute__((section(".entry"))) _start(unsigned long now,struct bc_ptr *ptr){
        char *l_str = (char *)ptr->malloc(16); //only really need 11, but oh well.
	ltostr(l_str,now);

        char *r_str = (char *)ptr->malloc(16);
	ltostr(r_str,1000);

	char *r2_str = (char *)ptr->malloc(16);
	ltostr(r2_str,500);



        return do_compare(do_mod(l_str,r_str,ptr),r2_str,ptr,COMPARE_LT);
}



static char * do_mod(char *left, char *right, struct bc_ptr *ptr){
	char *res_str;
	
	bc_num l = ptr->bc_new_num(10,10);
	bc_num r = ptr->bc_new_num(10,10);

	bc_num res = ptr->bc_new_num(0,0);

	ptr->bc_str2num(&l,left,10);
	ptr->bc_str2num(&r,right,10);


	ptr->bc_modulo(l,r,&res,0);
     
	ptr->bc_free_num(&l);
	ptr->bc_free_num(&r);

	res_str = ptr->bc_num2str(res);

	ptr->bc_free_num(&res);

        ptr->free(left);
        ptr->free(right);

	return res_str;
}


static char * do_add(char *left, char *right, struct bc_ptr *ptr){
	char *res_str;
	
	bc_num l = ptr->bc_new_num(10,10);
	bc_num r = ptr->bc_new_num(10,10);

	bc_num res = ptr->bc_new_num(0,0);

	ptr->bc_str2num(&l,left,10);
	ptr->bc_str2num(&r,right,10);


	ptr->bc_add(l,r,&res,0);
     
	ptr->bc_free_num(&l);
	ptr->bc_free_num(&r);

	res_str = ptr->bc_num2str(res);

	ptr->bc_free_num(&res);
        ptr->free(left);
        ptr->free(right);

	return res_str;
}

static char * do_sub(char *left, char *right, struct bc_ptr *ptr){
	char *res_str;
	
	bc_num l = ptr->bc_new_num(10,10);
	bc_num r = ptr->bc_new_num(10,10);

	bc_num res = ptr->bc_new_num(0,0);

	ptr->bc_str2num(&l,left,10);
	ptr->bc_str2num(&r,right,10);


	ptr->bc_sub(l,r,&res,0);
     
	ptr->bc_free_num(&l);
	ptr->bc_free_num(&r);

	res_str = ptr->bc_num2str(res);

	ptr->bc_free_num(&res);
        ptr->free(left);
        ptr->free(right);

	return res_str;
}

static char * do_mul(char *left, char *right, struct bc_ptr *ptr){
	char *res_str;
	
	bc_num l = ptr->bc_new_num(10,10);
	bc_num r = ptr->bc_new_num(10,10);

	bc_num res = ptr->bc_new_num(0,0);

	ptr->bc_str2num(&l,left,10);
	ptr->bc_str2num(&r,right,10);


	ptr->bc_multiply(l,r,&res,0);
     
	ptr->bc_free_num(&l);
	ptr->bc_free_num(&r);

	res_str = ptr->bc_num2str(res);

	ptr->bc_free_num(&res);
        ptr->free(left);
        ptr->free(right);
	return res_str;
}


static char * do_compare(char *left, char *right, struct bc_ptr *ptr,char type){
	char *res_str = ptr->malloc(4);
	
	bc_num l = ptr->bc_new_num(10,10);
	bc_num r = ptr->bc_new_num(10,10);

	ptr->bc_str2num(&l,left,10);
	ptr->bc_str2num(&r,right,10);

        int res = ptr->bc_compare(l,r);
        res_str[1] = 0;

        if((type == COMPARE_LT && res < 0)   ||
           (type == COMPARE_GT && res >0)    ||
           (type == COMPARE_LTE && res <= 0) ||
           (type == COMPARE_GTE && res >= 0) ||
           (type == COMPARE_E && res == 0)){
                res_str[0] = '1';
	}else{
                res_str[0] = '0';
	}
        ptr->bc_free_num(&l);
	ptr->bc_free_num(&r);

        ptr->free(left);
        ptr->free(right);

	return res_str;
}

