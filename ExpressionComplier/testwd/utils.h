#include <ctype.h>
#include <stdlib.h>

#define COMPARE_LT   1
#define COMPARE_GT   2
#define COMPARE_E    4
#define COMPARE_LTE  5 //1 or 4
#define COMPARE_GTE  6 //2 or 4

void my_free(void *ptr);
char * fix_pointer(char *ptr);

struct sdate {
  uint16_t y;
  uint8_t m;
  uint8_t d;

  uint8_t hours;
  uint8_t minutes;
  uint8_t seconds;
  uint8_t dow; //day of week, 0 = sunday, 3= wednesday, 6=saturday

  uint32_t epoch;
  uint16_t milliseconds;
};

struct operations_ptr {
   void * (*malloc)(unsigned int);
   void   (*free)(void *ptr);
   char * (*FP)(char *ptr);
   unsigned long (*millis)();
   char * (*ltostr) (long val);

   char * (*do_compare)(char *left, char *right,char type);
   char * (*do_add)(char *left, char *right);
   char * (*do_sub)(char *left, char *right);
   char * (*do_mul)(char *left, char *right);
   char * (*do_mod)(char *left, char *right);
   char * (*do_div)(char *left, char *right);
   char * (*do_pow)(char *left, char *right);

   char * (*do_negate)(char *right);

   char * (*do_and)(char *left, char *right);
   char * (*do_or)(char *left, char *right);
   char * (*do_not)(char *right);
   
   char * (*do_get_stored)(char *name);

   char (*isTrue)(char *val,char do_free);
   char (*isNumber)(char *val,char do_free);
};


