

char * get_stored_value(char *name,struct value_store *tree){
    int r = strncmp(name,tree->name,64);
    
    if(r == 0){
      return tree->value;
    }else if(r < 0 && tree->left != 0){
      return get_stored_value(name,tree->left);
    }else if(r > 0 && tree->right != 0){
      return get_stored_value(name,tree->right);
    }else{
      return 0;
    }
}

char * put_stored_value(char *name, char *value,struct value_store *tree){
    int r = strncmp(name,tree->name,64);
    
    if(r == 0){
      char *v = tree->value;
      tree->value = value;
      return v;
    }else if(r < 0){
      if(tree->left != 0){
        return put_stored_value(name,value,tree->left);
      }else{
        tree->left = new_value_store(name,value);
        return 0;
      }
    }else if(r > 0){
      if(tree->right != 0){
        return put_stored_value(name,value,tree->right);
      }else{
        tree->right = new_value_store(name,value);
        return 0;
      }
    }else{
      return 0;
    }
}

struct value_store *new_value_store(char *name,char *value){
  struct value_store *retval = (struct value_store *)malloc(sizeof(struct value_store));
  retval->name = name;
  retval->value = value;
  retval->left = 0;
  retval->right = 0;
  return retval;
}

