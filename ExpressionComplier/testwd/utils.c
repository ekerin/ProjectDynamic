#include "bcconfig.h"
#include <assert.h>
#include "number.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "utils.h"


#include "ValueTree.h"
#include "ValueTree.c"


#include "time.c"

struct value_store *stored_values;
unsigned long millis();

#include "number.c"
#include "DynamicOperations.c"

void setup_operations_ptr(struct operations_ptr *ptr);

//emulate the arduino millis command
unsigned long millis(){
  return 12345;
}

void my_free(void *ptr){
  //noop till I figure out how to differentiate malloc'd addresses from constants
}


char * fix_pointer(char *ptr){
  return ptr;
}


char *do_test(unsigned long now,struct operations_ptr *ptr);

int main(void){
  struct operations_ptr p;
  setup_operations_ptr(&p);

  bc_init_numbers();

  stored_values = new_value_store("duty","250");

  printf("%s\n",do_test(1474743412,&p));

}
