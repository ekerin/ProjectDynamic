/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thinkulator.dynamic;

import com.thinkulator.compiler.CompilationResult;
import com.thinkulator.compiler.ExpressionCLangCompiler;
import com.thinkulator.parser.ExpressionTokenizer;
import com.thinkulator.parser.ExpressionTreeBuilder;
import com.thinkulator.parser.ExpressionTreeBuilderException;
import com.thinkulator.parser.ParseTreeEntry;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author hack
 */
public class DynamicMain {
    public static void main(String [] args) throws IOException, ExpressionTreeBuilderException, ParseException{
        String calcFunc = args[0];
        
        
        ExpressionCLangCompiler instance = new ExpressionCLangCompiler();
//System.out.println(calcFunc);
        ExpressionTokenizer et = new ExpressionTokenizer(calcFunc);
        ExpressionTreeBuilder tb = ExpressionTreeBuilder.getBuilder(et);
        ParseTreeEntry pte = tb.parse();

        CompilationResult result = instance.compile(pte);
        
        
        File testWD = new java.io.File( "./builder" );
        
        //TODO: check that we have the right references
        //assertEquals("Number of references",1,result.getReferences().size());
        //assertEquals("Reference 0",result.getReferences().get(0),"X");
        //DEBUG, print the calculated script 
        File f = File.createTempFile("build_", ".c", testWD);
        //Create the program C source code.
        try (FileWriter fw = new FileWriter(f)) {
            fw.write("#include \"utils.h\"\n\n");
            fw.write("/*");
            fw.write(StringEscapeUtils.escapeJava(calcFunc.replace("*/", "*//*")));
            fw.write("*/\n");
            fw.write("char *  __attribute__ ((noinline)) __attribute__((section(\".entry\"))) _start(unsigned long now,struct operations_ptr *ptr){\n");

            fw.write("\treturn ");
            fw.write(result.getCalculationSource());
            fw.write(";\n");

            fw.write("}\n");
        }
        
        String fileName = f.getName();
        String targetName = fileName.split("\\.")[0]+".hex";
        
        StringBuilder compileResult = new StringBuilder();
        {
            ProcessBuilder builder = new ProcessBuilder(
                "/usr/bin/make","-s", targetName);
            builder.directory(testWD);
            builder.redirectErrorStream(true);
            Process p = builder.start();
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while (true) {
                line = r.readLine();
                if (line == null) { break; }
                compileResult.append(line);
                System.out.println(line);
            }
        }
        System.out.println("builder/"+targetName);
    }
}
