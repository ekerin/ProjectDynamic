/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.parser;

/**
 *
 * @author hack
 */
public class Token {
    private TokenType type;
    private String value;
    private int from;
    private int to;

    /** Create the token, extracted from the passed expression, from-to (inclusive of from, not inclusive of to)
     * 
     * @param type
     * @param expression
     * @param from - start position, inclusive
     * @param to - end position, exclusive
     */
    public Token(TokenType type,String expression,int from,int to){
        this.type = type;
        this.value = expression.substring(from,to);
        this.from = from;
        this.to = to;
    }

    /** Assume the value and from/to are correct.  This is used for strings where escape characters are present
     * 
     * @param type
     * @param expression
     * @param value
     * @param from
     * @param to
     */
    public Token(TokenType type,String expression,String value,int from,int to){
        this.type = type;
        this.value = value;
        this.from = from;
        this.to = to;
    }

    /**
     * @return the type
     */
    public TokenType getType() {
        return type;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @return the from
     */
    public int getFrom() {
        return from;
    }

    /**
     * @return the to
     */
    public int getTo() {
        return to;
    }

    public String toString(){
        return "From:"+getFrom()+" To:"+getTo()+" Type:"+getType().toString()+" Value:"+getValue();
    }
    
    public static enum TokenType{
        String,
        Number,
        Name,
        Operator
    }
}
