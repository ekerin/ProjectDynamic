/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.parser;

import java.util.ArrayList;

/**
 *
 * @author hack
 */
public class ParseTreeEntry {
    private String _id;
    private Token _token;
    private Symbol _symbol;

        /** Branches of the tree
     * 
     * For binary operators - left is first, right is second
     */
    private ArrayList<ParseTreeEntry> _branches;
    
    public ParseTreeEntry(String id, Token token, Symbol symbol) {
        this._token = token;
        this._symbol = symbol;
        this._id = id;
        _branches = new ArrayList<ParseTreeEntry>();
    }

    public ParseTreeEntry(String id, Symbol symbol) {
        this(id,null,symbol);
    }

    /**
     * @return the _id
     */
    public String getId() {
        return _id;
    }

    /**
     * @return the _token
     */
    public Token getToken() {
        return _token;
    }

    /**
     * @return the _branches
     */
    public ArrayList<ParseTreeEntry> getBranches() {
        return _branches;
    }

    public ParseTreeEntry getBranch(int i){
        return _branches.get(i);
    }
    
    public int getNumBranches(){
        return _branches.size();
    }


    public void addBranch(ParseTreeEntry pte){
        _branches.add(pte);
    }

    public void setBranch(int ord,ParseTreeEntry pte){
        while(ord >= _branches.size()){
            _branches.add(null);
        }
        _branches.set(ord,pte);
    }
    /**
     * @return the _symbol
     */
    public Symbol getSymbol() {
        return _symbol;
    }
}
