/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.parser;

import com.thinkulator.parser.Token.TokenType;
import com.thinkulator.parser.symbols.BasicSymbol;
import com.thinkulator.parser.symbols.FunctionCallSymbol;
import com.thinkulator.parser.symbols.InfixRightSymbol;
import com.thinkulator.parser.symbols.InfixSymbol;
import com.thinkulator.parser.symbols.ItselfSymbol;
import com.thinkulator.parser.symbols.PrefixSymbol;
import java.text.ParseException;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author hack
 */
public class ExpressionTreeBuilder {

    private ParseTreeEntry _currentEntry;
    private ExpressionTokenizer _tokens;
    private TreeMap<String,Symbol> _symbolTable;

    private ExpressionTreeBuilder(TreeMap<String,Symbol> symbolTable,ExpressionTokenizer tokens){
        this._symbolTable = symbolTable;
        _tokens = tokens;
    }

    private static TreeMap<String,Symbol> _baseSymbolTable;
    public synchronized static ExpressionTreeBuilder getBuilder(ExpressionTokenizer tokens){
        //share the base symbols between all of the expression tree builders (to save time and RAM)
        if(_baseSymbolTable == null){
            _baseSymbolTable = new TreeMap<String,Symbol>();
            //create the common symbols that mean little, but are used for separators and endings.
            _baseSymbolTable.put(",", new BasicSymbol(",",0));
            _baseSymbolTable.put(")", new BasicSymbol(")",0));
            _baseSymbolTable.put("]", new BasicSymbol("]",0)); //end of array

            //and some predefined utility tokens
            _baseSymbolTable.put("(end)", new BasicSymbol("(end)",0));
            _baseSymbolTable.put("(name)", new ItselfSymbol("(name)",0));
            _baseSymbolTable.put("(literal)", new ItselfSymbol("(literal)",0));

            //and some predefined literals
            _baseSymbolTable.put("true", new ItselfSymbol("true",0));
            _baseSymbolTable.put("false", new ItselfSymbol("false",0));

            //defines an array
            _baseSymbolTable.put("[",new BasicSymbol("[", 70){ //Please
                @Override
                public ParseTreeEntry getNud(ParseTreeEntry self, ExpressionTreeBuilder builder) throws ExpressionTreeBuilderException, ParseException {
                    if(!"]".equals(builder.getCurrentEntry().getId())){
                        while(true){
                            self.addBranch(builder.expression(0));

                            if(!("]".equals(builder.getCurrentEntry().getId())
                               || ",".equals(builder.getCurrentEntry().getId()))){
                                throw new ExpressionTreeBuilderException("Expected [(] or [,]",builder.getCurrentEntry().getToken());
                            }
                            if("]".equals(builder.getCurrentEntry().getId())){
                                break;
                            }
                            builder.advance(",");
                        }
                    }
                    builder.advance("]");
                    return self;
                }
            }); 
             
            _baseSymbolTable.put("(",new BasicSymbol("(", 71){ //Please
                @Override
                public ParseTreeEntry getNud(ParseTreeEntry self, ExpressionTreeBuilder builder) throws ExpressionTreeBuilderException, ParseException {
                    //parenthesis just force operator presedence, they aren't part of the tree
                    ParseTreeEntry pte = builder.expression(0);
                    builder.advance(")");
                    return pte;
                }
            }); 
            _baseSymbolTable.put("^",new InfixSymbol("^",70)); //Excuse
            _baseSymbolTable.put("*",new InfixSymbol("*",60)); //My
            _baseSymbolTable.put("/",new InfixSymbol("/",60)); //Dear
            _baseSymbolTable.put("%",new InfixSymbol("%",60));
            _baseSymbolTable.put("+",new InfixSymbol("+",50)); //Aunt
            _baseSymbolTable.put("-",new InfixSymbol("-",50){ //Sally
                //minus is both an infix and prefix depending on location, also give it a Nud operation
                @Override
                public ParseTreeEntry getNud(ParseTreeEntry self, ExpressionTreeBuilder builder) throws ExpressionTreeBuilderException, ParseException {
                    self.setBranch(0, builder.expression(70));
                    return self;
                }
            }); 

            _baseSymbolTable.put("AND",new InfixRightSymbol("AND", 30));
            _baseSymbolTable.put("OR",new InfixRightSymbol("OR", 30));
            _baseSymbolTable.put("NOT",new PrefixSymbol("NOT", 0));

            _baseSymbolTable.put("=",new InfixRightSymbol("=", 40));
            _baseSymbolTable.put("!=",new InfixRightSymbol("!=", 40));
            _baseSymbolTable.put("<>",new InfixRightSymbol("<>", 40));
            _baseSymbolTable.put("<",new InfixRightSymbol("<", 40));
            _baseSymbolTable.put("<=",new InfixRightSymbol("<=", 40));
            _baseSymbolTable.put(">",new InfixRightSymbol(">", 40));
            _baseSymbolTable.put(">=",new InfixRightSymbol(">=", 40));
            _baseSymbolTable.put("==",new InfixRightSymbol("==", 40));
            _baseSymbolTable.put("!==",new InfixRightSymbol("!==", 40));
            
            _baseSymbolTable.put("IF",new FunctionCallSymbol("IF",0));
            _baseSymbolTable.put("LEFT",new FunctionCallSymbol("LEFT",0));
            _baseSymbolTable.put("RIGHT",new FunctionCallSymbol("RIGHT",0));
            _baseSymbolTable.put("MID",new FunctionCallSymbol("MID",0));
            _baseSymbolTable.put("LEN",new FunctionCallSymbol("LEN",0));
            _baseSymbolTable.put("TRIM",new FunctionCallSymbol("TRIM",0));
            _baseSymbolTable.put("LOWER",new FunctionCallSymbol("LOWER",0));
            _baseSymbolTable.put("UPPER",new FunctionCallSymbol("UPPER",0));
           
            _baseSymbolTable.put("IPMT",new FunctionCallSymbol("IPMT",0));
           
            //_baseSymbolTable.put("AVERAGE",new FunctionCallSymbol("AVERAGE",0));
            //_baseSymbolTable.put("TODAY",new FunctionCallSymbol("TODAY",0));
            _baseSymbolTable.put("SQRT",new FunctionCallSymbol("SQRT",0));
            _baseSymbolTable.put("SUM",new FunctionCallSymbol("SUM",0));
            _baseSymbolTable.put("COUNT",new FunctionCallSymbol("COUNT",0));
            
            _baseSymbolTable.put("UPTIME",new FunctionCallSymbol("UPTIME",0));
        }

        //make a copy of the symbol table for this new tokenizer
        TreeMap<String,Symbol> mySymbolTable = new TreeMap<String,Symbol>();
        for(Entry<String,Symbol> e:_baseSymbolTable.entrySet()){
            mySymbolTable.put(e.getKey(),e.getValue());
        }

        return new ExpressionTreeBuilder(mySymbolTable,tokens);
    }

    public ParseTreeEntry parse() throws ExpressionTreeBuilderException, ParseException{
        advance();
        ParseTreeEntry retval = expression(0);
        advance("(end)");

        return retval;
    }

    public void advance() throws ExpressionTreeBuilderException, ParseException{
        advance(null);
    }

    /** Asserted advance, the current token better be the ID passed, or something is mal-formatted
     * 
     * @param id Check the current token against this ID.  If they match, we're good.  If not throw.
     * @throws ExpressionTreeBuilderException If the passed token is wrong for the current token, or the exception is thrown by one of the parsed operators.
     */
    public void advance(String id) throws ExpressionTreeBuilderException, ParseException{
        if(id != null && !id.equals(_currentEntry.getId()) ){
            throw new ExpressionTreeBuilderException("Expected ["+id+"] was ["+_currentEntry.getId()+"]",_currentEntry.getToken());
        }
        //see if there are any more tokens to get
        if(!_tokens.hasNext()){
            _currentEntry = new ParseTreeEntry("(end)", _symbolTable.get("(end)"));
            return;
        }
        Token token = _tokens.next();

        if(token.getType() == TokenType.Name){
            //check to see if the name exists in the symbol table, if so use that, we use this for constants, functions, operators w/ names
            if(_symbolTable.containsKey(token.getValue())){
                _currentEntry = new ParseTreeEntry(token.getValue(), token, _symbolTable.get(token.getValue()));
            }else{
                _currentEntry = new ParseTreeEntry(token.getValue(), token, _symbolTable.get("(name)"));
            }
        }else if(token.getType() == TokenType.Operator){
            Symbol sym = _symbolTable.get(token.getValue());
            if(sym == null){
                throw new ExpressionTreeBuilderException("Unknown operator ["+sym+"]",token);
            }
            _currentEntry = new ParseTreeEntry(token.getValue(),token,sym);
        }else if(token.getType() == TokenType.String || token.getType() == TokenType.Number){
            //literals return the parse tree entry they are defined for for nud.
            //so we handle them special-like.
            _currentEntry = new ParseTreeEntry(token.getValue(), token, _symbolTable.get("(literal)"));
        }else {
            throw new ExpressionTreeBuilderException("Unexpected token type ["+token.getType()+"]",token);
        }
    }


    public ParseTreeEntry expression(int rightBindingPriority) throws ExpressionTreeBuilderException, ParseException{
        ParseTreeEntry left;
        ParseTreeEntry c = _currentEntry;
        advance();
        left = c.getSymbol().getNud(c,this);
        while(rightBindingPriority < _currentEntry.getSymbol().getLeftBindingPriority()){
            c = _currentEntry;
            advance();
            left = c.getSymbol().getLed(c,this, left);
        }
        return left;
    }

    public ParseTreeEntry getCurrentEntry(){
        return _currentEntry;
    }
}
