/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.parser.symbols;

import com.thinkulator.parser.ExpressionTreeBuilder;
import com.thinkulator.parser.ExpressionTreeBuilderException;
import com.thinkulator.parser.ParseTreeEntry;
import java.text.ParseException;

/**
 *
 * @author hack
 */
public class InfixSymbol extends BasicSymbol {
    public InfixSymbol(String id,int bindingPriority){
        super(id,bindingPriority);
    }

    @Override
    public ParseTreeEntry getLed(ParseTreeEntry self, ExpressionTreeBuilder builder, ParseTreeEntry left) throws ExpressionTreeBuilderException, ParseException {
        self.setBranch(0, left);
        self.setBranch(1, builder.expression(this.getLeftBindingPriority()));
        return self;
    }


}
