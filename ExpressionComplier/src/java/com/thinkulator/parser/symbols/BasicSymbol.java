/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.parser.symbols;

import com.thinkulator.parser.ExpressionTreeBuilder;
import com.thinkulator.parser.ExpressionTreeBuilderException;
import com.thinkulator.parser.ParseTreeEntry;
import com.thinkulator.parser.Symbol;
import java.text.ParseException;

/**
 *
 * @author hack
 */
public class BasicSymbol extends Symbol {

    public BasicSymbol(String id,int leftBindingPriority){
        super(id,leftBindingPriority);
    }
    
    @Override
    public ParseTreeEntry getNud(ParseTreeEntry self, ExpressionTreeBuilder builder) throws ExpressionTreeBuilderException, ParseException {
        throw new ExpressionTreeBuilderException("Undefined",self.getToken());
    }

    @Override
    public ParseTreeEntry getLed(ParseTreeEntry self, ExpressionTreeBuilder builder,ParseTreeEntry left) throws ExpressionTreeBuilderException, ParseException {
        throw new ExpressionTreeBuilderException("Missing operator",self.getToken());
    }

}
