/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.parser;

/**
 *
 * @author hack
 */
public class ExpressionTreeBuilderException extends Exception{

    private Token token;
    
    public ExpressionTreeBuilderException(String msg) {
        super(msg);
    }
    
    public ExpressionTreeBuilderException(String msg, Token tok) {
        super(msg+" ["+tok.getValue()+"] at "+tok.getFrom()+" to "+tok.getTo());
        this.token = tok;
    }

    public ExpressionTreeBuilderException(String msg,Throwable t, Token tok) {
        super(msg+" ["+tok.getValue()+"] at "+tok.getFrom()+" to "+tok.getTo(),t);
        this.token = tok;
    }

    public ExpressionTreeBuilderException(Throwable t, Token tok) {
        super(t);
        this.token = tok;
    }

    public Token getToken(){
        return this.token;
    }

    public String toString(){
        if(token == null){
            return super.toString();
        }else{
            return super.toString()+" Token:"+token.toString();
        }
        
    }
}
