/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.compiler;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hack
 */
public class CompilationResult {
    private String _calculationSource;
    private List<String> _references;

    public CompilationResult(String language) {
        _references = new ArrayList<String>();
    }

    public String getCalculationSource() {
        return _calculationSource;
    }

    public void setCalculationSource(String calculationJavascript) {
        _calculationSource = calculationJavascript;
    }

    public void addReference(String fieldName){
        _references.add(fieldName);
    }

    public List<String> getReferences() {
        return _references;
    }
}
