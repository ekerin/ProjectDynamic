/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.thinkulator.compiler;

import com.thinkulator.parser.ParseTreeEntry;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author hack
 */
public class ExpressionJavascriptCompiler {
    private Map<String,FunctionCompiler> _functions;

    public ExpressionJavascriptCompiler(){
        _functions = new TreeMap<String,FunctionCompiler>(String.CASE_INSENSITIVE_ORDER);

        /** Don't forget to add an entry to ExpreessionTreeBuilder.java */
       _functions.put("SQRT",new FunctionCompiler() {
            @RequiredBranches(1)
            public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                //TODO: Find/build a BigDecimal version of Square root...
                sb.append("String(Math.sqrt(Number(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(")))");
            }
        });
        _functions.put("IF",new FunctionCompiler() {
            @RequiredBranches(3)
            public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("((");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(")?(");
                compiler.compile(pte.getBranches().get(1),sb,result);
                sb.append("):(");
                compiler.compile(pte.getBranches().get(2),sb,result);
                sb.append("))");
            }
        });
        _functions.put("LEFT",new FunctionCompiler() {
            @RequiredBranches(2)
            public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").substring(0,Number(");
                compiler.compile(pte.getBranches().get(1),sb,result);
                sb.append(")))");
            }
        });
        _functions.put("MID",new FunctionCompiler() {
            @RequiredBranches(3)
            public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").substr(Number(");
                compiler.compile(pte.getBranches().get(1),sb,result);
                sb.append(")-1,Number(");
                compiler.compile(pte.getBranches().get(2),sb,result);
                sb.append(")))");
            }
        });
        _functions.put("RIGHT",new FunctionCompiler() {
            @RequiredBranches(2)
            public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").substring(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").length-Number(");
                compiler.compile(pte.getBranches().get(1),sb,result);
                sb.append(")))");
            }
        });
        _functions.put("LEN",new FunctionCompiler() {
            @RequiredBranches(1)
            public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").length)");
            }
        });
        _functions.put("TRIM",new FunctionCompiler() {
            @RequiredBranches(1)
            public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").trim())");
            }
        });
        _functions.put("LOWER",new FunctionCompiler() {
            @RequiredBranches(1)
            public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").toLowerCase())");
            }
        });
        _functions.put("UPPER",new FunctionCompiler() {
            @RequiredBranches(1)
            public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(String(");
                compiler.compile(pte.getBranches().get(0),sb,result);
                sb.append(").toUpperCase())");
            }
        });
        _functions.put("SUM",new FunctionCompiler() {
            @RequiredBranches(value=1,min=true)
            public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(function(){var va=[];");
                List<ParseTreeEntry> branches = pte.getBranches();
                sb.append("va=va.concat(");
                boolean first=true;
                for(ParseTreeEntry pti:branches){
                    if(first){
                        first=false;
                    }else{
                        sb.append(",");
                    }
                    compiler.compile(pti,sb,result);
                }
                sb.append(");");
                sb.append("var r=new BigDecimal('0');");
                sb.append("for(var i=0;i<va.length;i++){");
                sb.append("r=r.add(new BigDecimal(String(va[i])))");
                sb.append("}");
                sb.append("return String(r);})()");
            }
        });
        _functions.put("COUNT",new FunctionCompiler() {
            @RequiredBranches(value=1,min=true)
            public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte, StringBuilder sb, CompilationResult result) {
                sb.append("(function(){var va=[];");
                List<ParseTreeEntry> branches = pte.getBranches();
                sb.append("va=va.concat(");
                boolean first=true;
                for(ParseTreeEntry pti:branches){
                    if(first){
                        first=false;
                    }else{
                        sb.append(",");
                    }
                    compiler.compile(pti,sb,result);
                }
                sb.append(");");
                sb.append("var r=new BigDecimal('0');");
                sb.append("for(var i=0;i<va.length;i++){");
                sb.append("if($.isNumeric(va[i])){");
                sb.append("r=r.add(new BigDecimal('1'))");
                sb.append("}}");
                sb.append("return String(r);})()");
            }
        });
    }

    public CompilationResult compile(ParseTreeEntry pte){
        CompilationResult result = new CompilationResult("javascript");

        StringBuilder sb = new StringBuilder();
        compile(pte,sb,result);
        result.setCalculationSource(sb.toString());
        
        return result;
    }

    protected void compile(ParseTreeEntry pte,StringBuilder sb,CompilationResult result){
        String symbolID = pte.getSymbol().getID();
        if( "-".equals(symbolID) && pte.getNumBranches() == 1){
            //This is a negation, not a subtraction
           sb.append("Number(-1*");
           compile(pte.getBranches().get(0),sb,result);
           sb.append(")");
                
        }else if(   "+".equals(symbolID)
           || "-".equals(symbolID)
           || "*".equals(symbolID)
           || "%".equals(symbolID)
           //|| "/".equals(symbolID)
                ){
            //we'll start w/ just supporting numbers.  Later we'll move on to text too
            if(pte.getNumBranches() != 2){
                //TODO: Exception
            }
            
            sb.append("String(new BigDecimal(");
            compile(pte.getBranches().get(0),sb,result);
            sb.append(")");
            if("+".equals(symbolID)){
                sb.append(".add");
            }else if("-".equals(symbolID)){
                sb.append(".subtract");
            }else if("*".equals(symbolID)){
                sb.append(".multiply");
            }else if("/".equals(symbolID)){
                sb.append(".divide");
            }else if("%".equals(symbolID)){
                sb.append(".remainder");
            }
            sb.append("(new BigDecimal(");
            compile(pte.getBranches().get(1),sb,result);
            sb.append(")))");

        }else if("/".equals(symbolID)){
            //TODO: A BigDecimal version. By default, the BigDecimal significant digits matches the numerator. So 1/3 is 0, and 2/3 is 1 due to rounding, instead of 0.(3) and 0.(6)
            //we'll start w/ just supporting numbers.  Later we'll move on to text too
            if(pte.getNumBranches() != 2){
                //TODO: Exception
            }

            sb.append("String(Number(");
            compile(pte.getBranches().get(0),sb,result);
            sb.append(")/Number(");
            compile(pte.getBranches().get(1),sb,result);
            sb.append("))");
        }else if("^".equals(symbolID)
                || "POWER".equalsIgnoreCase(symbolID)){
            if(pte.getNumBranches() != 2){
                //TODO: Exception
            }

            sb.append("String(new BigDecimal(");
            compile(pte.getBranches().get(0),sb,result);
            sb.append(").pow(new BigDecimal(");
            compile(pte.getBranches().get(1),sb,result);
            sb.append(")))");
        }else if("[".equals(symbolID)){
            sb.append("[");
            for(int i=0;i<pte.getNumBranches();i++){
                if(i>0){
                    sb.append(",");
                }
                sb.append("(");
                compile(pte.getBranches().get(i),sb,result);
                sb.append(")");
            }
            sb.append("]");
        
        }else if("AND".equals(symbolID)){
            if(pte.getNumBranches() != 2){
                //TODO: Exception
            }
            sb.append("(Boolean(");
            compile(pte.getBranches().get(0),sb,result);
            sb.append(") && Boolean(");
            compile(pte.getBranches().get(1),sb,result);
            sb.append("))");
        }else if("OR".equals(symbolID)){
            if(pte.getNumBranches() != 2){
                //TODO: Exception
            }
            sb.append("(Boolean(");
            compile(pte.getBranches().get(0),sb,result);
            sb.append(") || Boolean(");
            compile(pte.getBranches().get(1),sb,result);
            sb.append("))");
        }else if("NOT".equals(symbolID)){
            if(pte.getNumBranches() != 1){
                //TODO: Exception
            }
            sb.append("(!Boolean(");
            compile(pte.getBranches().get(0),sb,result);
            sb.append("))");
        }else if("true".equals(symbolID)){
            sb.append("true");
        }else if("false".equals(symbolID)){
            sb.append("false");
        }else if("(literal)".equals(pte.getSymbol().getID())){
            sb.append("'");
            sb.append(StringEscapeUtils.escapeEcmaScript(pte.getToken().getValue()));
            sb.append("'");
        }else if("(name)".equals(pte.getSymbol().getID())){
            sb.append("FV('");  //FV is a reference to adder.js->findValue
            sb.append(StringEscapeUtils.escapeEcmaScript(pte.getToken().getValue()));
            sb.append("',dtp)");
            result.addReference(pte.getToken().getValue());
        }else{
            //call a registered handler for a function
            if(_functions.containsKey(pte.getSymbol().getID())){
                try {
                    int [] numRequiredBranches = null;
                    boolean isMin = false;
                    //check for the annotations used to mark requirements and other information
                    FunctionCompiler fc = _functions.get(pte.getSymbol().getID());


                    Method m = fc.getClass().getMethod("compile", ExpressionJavascriptCompiler.class, ParseTreeEntry.class, StringBuilder.class, CompilationResult.class);
                    if (m.isAnnotationPresent(RequiredBranches.class)) {
                        RequiredBranches c = m.getAnnotation(RequiredBranches.class);
                        isMin = c.min();
                        numRequiredBranches = c.value();

                    }

                    if(numRequiredBranches != null){
                        boolean okay=false;
                        if(isMin){
                            if(pte.getNumBranches() >= numRequiredBranches[0]){
                                okay=true;
                            }
                        }else{
                            for(int i=0;i<numRequiredBranches.length;i++){
                                if(pte.getNumBranches() == numRequiredBranches[i]){
                                    okay=true;
                                }
                            }
                        }
                        if(!okay){
                            //TODO:throw an exception about not enough parameters
                        }
                    }
                    _functions.get(pte.getSymbol().getID()).compile(this, pte, sb, result);

                } catch (NoSuchMethodException ex) {
                    throw new IllegalArgumentException("FunctionCompilers must have the compile method...", ex);
                } catch (SecurityException ex) {
                    throw new IllegalArgumentException("FunctionCompilers must have the compile method declared public...", ex);
                }
            }
        }
    }

    public static interface FunctionCompiler{
        public void compile(ExpressionJavascriptCompiler compiler,ParseTreeEntry pte,StringBuilder sb,CompilationResult result);
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public static @interface RequiredBranches{
        public int [] value();
        public boolean min() default false;
    }
}
