#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <stdint.h>
#include "utils.h"


int main(int argc,char *argv[]){
	uint8_t header[BLOCK_HEADER_SIZE];
	memset(header,0,BLOCK_HEADER_SIZE);	
	int binary_fd = open(argv[1],O_RDWR);
	if(binary_fd == -1){
		printf("Open Failed\n");
		return 1;
        }        
	ssize_t by = read(binary_fd,header,BLOCK_HEADER_SIZE);
	if(by < BLOCK_HEADER_SIZE){
		printf("Header read failed\n");
		return 2;
	}

	block_header *myHeader;
	myHeader = (block_header *) &header;	
        printf("---BEGIN HEADER INJECTION---\n");
	printf("{\"current\":{\n");
	printf("\t\"type\": \"%d\"\n",myHeader->block_type);
	printf("\t,\"numBlocks\": \"%d\"\n",myHeader->block_size);
	printf("\t,\"entryOffset\": \"%d\"\n",(uint32_t)myHeader->entry_offset);
	printf("\t,\"setupOffset\": \"%d\"\n",(uint32_t)myHeader->setup_offset);
	printf("\t,\"SHA256_sig\": \"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x\"}\n",
		myHeader->sha256_sig[0],
		myHeader->sha256_sig[1],
		myHeader->sha256_sig[2],
		myHeader->sha256_sig[3],
		myHeader->sha256_sig[4],
		myHeader->sha256_sig[5],
		myHeader->sha256_sig[6],
		myHeader->sha256_sig[7],
		myHeader->sha256_sig[8],
		myHeader->sha256_sig[9],
		myHeader->sha256_sig[10],
		myHeader->sha256_sig[11],
		myHeader->sha256_sig[12],
		myHeader->sha256_sig[13],
		myHeader->sha256_sig[14],
		myHeader->sha256_sig[15],
		myHeader->sha256_sig[16],
		myHeader->sha256_sig[17],
		myHeader->sha256_sig[18],
		myHeader->sha256_sig[19],
		myHeader->sha256_sig[20],
		myHeader->sha256_sig[21],
		myHeader->sha256_sig[22],
		myHeader->sha256_sig[23],
		myHeader->sha256_sig[24],
		myHeader->sha256_sig[25],
		myHeader->sha256_sig[26],
		myHeader->sha256_sig[27],
		myHeader->sha256_sig[28],
		myHeader->sha256_sig[29],
		myHeader->sha256_sig[30],
		myHeader->sha256_sig[31]);

	
	//read the SHA256 sum from STDIN for now
	read(STDIN_FILENO,myHeader->sha256_sig,32);

        struct stat statBuf;

	fstat(binary_fd,&statBuf);

	int blocks = statBuf.st_size/512;
        if(statBuf.st_size%512 >0){
		blocks++;
	}

	myHeader->block_size = blocks;

        printf(",\"new\":{\n");
        printf("\t\"type\": \"%d\"\n",myHeader->block_type);
        printf("\t,\"numBlocks\": \"%d\"\n",myHeader->block_size);
        printf("\t,\"entryOffset\": \"%d\"\n",(uint32_t)myHeader->entry_offset);
        printf("\t,\"setupOffset\": \"%d\"\n",(uint32_t)myHeader->setup_offset);
        printf("\t,\"SHA256_sig\": \"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x\"}\n",
		myHeader->sha256_sig[0],
		myHeader->sha256_sig[1],
		myHeader->sha256_sig[2],
		myHeader->sha256_sig[3],
		myHeader->sha256_sig[4],
		myHeader->sha256_sig[5],
		myHeader->sha256_sig[6],
		myHeader->sha256_sig[7],
		myHeader->sha256_sig[8],
		myHeader->sha256_sig[9],
		myHeader->sha256_sig[10],
		myHeader->sha256_sig[11],
		myHeader->sha256_sig[12],
		myHeader->sha256_sig[13],
		myHeader->sha256_sig[14],
		myHeader->sha256_sig[15],
		myHeader->sha256_sig[16],
		myHeader->sha256_sig[17],
		myHeader->sha256_sig[18],
		myHeader->sha256_sig[19],
		myHeader->sha256_sig[20],
		myHeader->sha256_sig[21],
		myHeader->sha256_sig[22],
		myHeader->sha256_sig[23],
		myHeader->sha256_sig[24],
		myHeader->sha256_sig[25],
		myHeader->sha256_sig[26],
		myHeader->sha256_sig[27],
		myHeader->sha256_sig[28],
		myHeader->sha256_sig[29],
		myHeader->sha256_sig[30],
		myHeader->sha256_sig[31]);
        printf("}\n---END HEADER INJECTION---\n");

	//write the new header block to the beginning of the file
	pwrite(binary_fd,header,BLOCK_HEADER_SIZE,0);
	
	close(binary_fd);
}

/*
block_header myHeader __attribute__((section(".header"))) = {
	.block_type = 0x01,
        .block_size = 0x00,
        .entry_offset = &_start,
        .setup_offset = 0x00,
        .sha256_sig = 0x00
};*/
