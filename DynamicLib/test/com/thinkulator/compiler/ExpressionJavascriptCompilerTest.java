/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package com.thinkulator.compiler;

import com.thinkulator.compiler.ExpressionTestRunner.ExpressionTest;
import com.thinkulator.parser.ExpressionTokenizer;
import com.thinkulator.parser.ExpressionTreeBuilder;
import com.thinkulator.parser.ExpressionTreeBuilderException;
import com.thinkulator.parser.ParseTreeEntry;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Map;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import org.junit.runner.RunWith;

/**
 *
 * @author hack
 */
@RunWith(ExpressionTestRunner.class)
public class ExpressionJavascriptCompilerTest {

    public ExpressionJavascriptCompilerTest() {
    }

    public ExpressionTest [] getExpressionTests(){
        return new ExpressionTest[]{
            new ExpressionTest("Constant - Positives","3","3"),
            new ExpressionTest("Constant - Negatives","-3","-3"),
            new ExpressionTest("Constant - Decimals","3.3","3.3"),
            new ExpressionTest("Constant - Decimals no leading number",".3",".3"),
            new ExpressionTest("Addition - Integers","1+2","3"),
            new ExpressionTest("Addition - Decimals","1.01+1.99","3.00"),
            new ExpressionTest("Subtraction - Integers","1-2","-1"),
            new ExpressionTest("Subtraction - Decimals","1.99-1.01","0.98"),
            new ExpressionTest("Multiplication - Integers","3*2","6"),
            new ExpressionTest("Multiplication - Decimals","1.99*1.01","2.0099"),
            new ExpressionTest("Division - Integers","3/2","1.5"),
            new ExpressionTest("Division - Decimals","1.99/1.01","1.9702970297029703"),
            new ExpressionTest("Exponent - Integers","3^2","9"),
            new ExpressionTest("Exponent - Decimal Base, Integer Power","1.99^2","3.9601"),
            new ExpressionTest("Modulus - Integers","3%2","1"),
            new ExpressionTest("Modulus - Decimals, no wrap","1.99%2","1.99"),
            new ExpressionTest("Modulus - Decimal, wrap","3.97%2","1.97"),
            new ExpressionTest("Order of Operations #1", "((10+4)-(24/2)^2)*7^2", "-6370"),
            new ExpressionTest("Order of Operations #2","3+(2+(4+3)^2)+4","58"),
            new ExpressionTest("Order of Operations #3","((9-6)^2*6)-9-2^2","41"),
            new ExpressionTest("Order of Operations #4","(20/10)^2+((13-2)+4^2)","31"),
            new ExpressionTest("Order of Operations #5","7+(10+(6+5)^2)+7","145"),
            new ExpressionTest("Order of Operations #6","(3^2+(16/4+3^2))+2^2","26"),
            new ExpressionTest("Order of Operations #7","((13+3)-(15/3)^2)+7^2","40"),
            new ExpressionTest("Order of Operations #8","((9-4)^2*7)-4-2^2","167"),
            new ExpressionTest("Order of Operations #9","(16/8)^2+((12+7)+5^2)","48"),
            new ExpressionTest("Order of Operations #10","(3^2+(8/2+5^2))-5^2","13"),
            new ExpressionTest("Order of Operations #11","6/2*(2+1)","9"),  //http://www.reddit.com/r/WTF/comments/2i03tm/we_had_this_argument_for_hours_and_when_we/
            
            new ExpressionTest("Logic - AND","true AND false","false"),
            new ExpressionTest("Logic - AND","false AND false","false"),
            new ExpressionTest("Logic - AND","true AND true","true"),
            new ExpressionTest("Logic - OR","true OR false","true"),
            new ExpressionTest("Logic - OR","true OR true","true"),
            new ExpressionTest("Logic - OR","false OR false","false"),
            new ExpressionTest("Logic - NOT","NOT false","true"),
            new ExpressionTest("Logic - NOT","NOT true","false"),
            new ExpressionTest("Function - IF","IF(true,1,2)","1"),
            new ExpressionTest("Function - IF","IF(false,1,2)","2"),
            new ExpressionTest("Function - IF","IF(true AND false,1,2)","2"),
            new ExpressionTest("Function - IF","IF(true AND true,1,2)","1"),
            new ExpressionTest("Function - IF","IF(true,1+1,3)","2"),
            new ExpressionTest("Function - IF","IF(false,1,1+1)","2"),
            new ExpressionTest("Function - LEFT","LEFT('foo',2)","fo"),
            new ExpressionTest("Function - LEFT","LEFT('',3)",""),
            new ExpressionTest("Function - LEFT","LEFT('fo',3)","fo"),
            new ExpressionTest("Function - LEFT","LEFT('foo',0)",""),
            new ExpressionTest("Function - LEFT","LEFT('foo',-1)",""),
            new ExpressionTest("Function - MID","MID('foobarbiz',1,2)","fo"),
            new ExpressionTest("Function - MID","MID('foobarbiz',4,3)","bar"),
            new ExpressionTest("Function - MID","MID('foobarbiz',8,3)","iz"),
            new ExpressionTest("Function - MID","MID('foobarbiz',8,0)",""),
            new ExpressionTest("Function - MID","MID('foobarbiz',8,-1)",""),
            new ExpressionTest("Function - RIGHT","RIGHT('foo',2)","oo"),
            new ExpressionTest("Function - RIGHT","RIGHT('',3)",""),
            new ExpressionTest("Function - RIGHT","RIGHT('fo',3)","fo"),
            new ExpressionTest("Function - RIGHT","RIGHT('foo',0)",""),
            new ExpressionTest("Function - RIGHT","RIGHT('foo',-1)",""),
            new ExpressionTest("Function - LEN","LEN('foo')","3"),
            new ExpressionTest("Function - LEN","LEN('fo')","2"),
            new ExpressionTest("Function - LEN","LEN('')","0"),
            new ExpressionTest("Function - TRIM","TRIM(' foo ')","foo"),
            new ExpressionTest("Function - TRIM","TRIM('foo')","foo"),
            new ExpressionTest("Function - LOWER","LOWER('FOO')","foo"),
            new ExpressionTest("Function - LOWER","LOWER('foo')","foo"),
            new ExpressionTest("Function - UPPER","UPPER('FOO')","FOO"),
            new ExpressionTest("Function - UPPER","UPPER('foo')","FOO"),
            
            
        /*    new ExpressionTest("Function - IPMT","IPMT(0.04/12,1,20*12,164000)","546.67"),
            new ExpressionTest("Function - IPMT","IPMT(0.04/12,30,20*12,164000)","501.36"),
            new ExpressionTest("Function - IPMT","IPMT(0.04/12,119,20*12,164000)","331.62"),
            new ExpressionTest("Function - IPMT","IPMT(0.04/12,240,20*12,164000)","3.30"),*/

            new ExpressionTest("Function - SUM","SUM(1,2,3,4)","10"),
            new ExpressionTest("Function - SUM","SUM([1,2,3,4])","10"),
            new ExpressionTest("Function - COUNT","COUNT(1,2,3,4)","4"),   //needs jQuery
            new ExpressionTest("Function - COUNT","COUNT(1,'A',3,4)","3"), //needs jQuery
            
            
            new ExpressionTest("Literals - Escape Sequences","'\\\\'","\\"),
            
            
                        
            
            new ExpressionTest("BIG Expression","(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)+(1+2+3+4+5+6+7+8+9+10)","1100")
        };
    }
    
    
    public static String executeFunction(String calcFunc, Map<String,Object> data) throws FileNotFoundException, ScriptException, ExpressionTreeBuilderException, ParseException{
        ExpressionJavascriptCompiler instance = new ExpressionJavascriptCompiler();

        ExpressionTokenizer et = new ExpressionTokenizer(calcFunc);
        ExpressionTreeBuilder tb = ExpressionTreeBuilder.getBuilder(et);
        ParseTreeEntry pte = tb.parse();

        CompilationResult result = instance.compile(pte);
        //TODO: check that we have the right references
        //assertEquals("Number of references",1,result.getReferences().size());
        //assertEquals("Reference 0",result.getReferences().get(0),"X");
        //DEBUG, print the calculated script 
        System.out.println(result.getCalculationSource());
        
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        
        //load the BigDecimal Library
        engine.eval(new InputStreamReader(new FileInputStream(new File("./web/static/BigDecimal.js"))));
        
        engine.eval("function FV(name,data){return data[name]}"); //TODO, this does not properly implement repeating data, fix that.
        //TODO: turn data into something dtp can use, or re-implement FV to use data;

        engine.eval("var dtp = {};");
        return engine.eval(result.getCalculationSource()).toString();
    }
    
}