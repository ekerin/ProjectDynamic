#include <ctype.h>
#include <stdlib.h>

#define COMPARE_LT   1
#define COMPARE_GT   2
#define COMPARE_E    4
#define COMPARE_LTE  5 //1 or 4
#define COMPARE_GTE  6 //2 or 4

#define PART_CODE_YEAR  1
#define PART_CODE_MONTH 2
#define PART_CODE_DAY   4
#define PART_CODE_HOUR  8
#define PART_CODE_MIN   16
#define PART_CODE_SEC   32
#define PART_CODE_MSEC  64
#define PART_CODE_DOW   128


void my_free(void *ptr);
char * fix_pointer(char *ptr);

struct operations_ptr {
   void * (*malloc)(unsigned int);
   void   (*free)(void *ptr);
   char * (*FP)(char *ptr);
   unsigned long (*millis)();
   char * (*ltostr) (long val);

   char (*isTrue)(char *val,char do_free);
   char (*isNumber)(char *val,char do_free);

   char * (*do_compare)(char *left, char *right,char type);  //type define prefix = COMPARE_
   char * (*do_add)(char *left, char *right);
   char * (*do_sub)(char *left, char *right);
   char * (*do_mul)(char *left, char *right);
   char * (*do_mod)(char *left, char *right);
   char * (*do_div)(char *left, char *right);
   char * (*do_pow)(char *left, char *right);

   char * (*do_negate)(char *right);

   char * (*do_and)(char *left, char *right);
   char * (*do_or)(char *left, char *right);
   char * (*do_not)(char *right);
   char * (*do_timer)(char *name, char *value, char *timeout);
    
   char * (*do_get_stored)(char *name);
   char * (*do_put_stored)(char *name,char *value);

   char * (*do_date_part)(char *date,int part_code);  //part_code define prefix = PART_CODE_
};

#define BLOCK_HEADER_SIZE 128
typedef struct __attribute__((packed, aligned(4))) {
   uint8_t block_type;  //FF = blank page, 0x01 = formula, 0x02 = output_handler, 0x03 = input_handler
   uint8_t reserved;
   uint16_t block_size; //In 512 byte segments

   uint8_t sha256_sig[32]; //starts 128 bytes in, goes until the end of the block, counting any trailing zero padding to fill a whole 512 byte segment
   
   uint32_t entry_offset; //offset from beginning of block for execution function
   uint32_t setup_offset; //offset from beginning of block for setup function
   
} block_header;


