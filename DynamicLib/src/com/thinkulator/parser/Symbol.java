/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package com.thinkulator.parser;

import java.text.ParseException;

/**
 *
 * @author hack
 */
public abstract class Symbol {
    private String _id;
    private int    _leftBindingPriority = 0;
    
    public Symbol(String id,int leftBindingPriority){
        _id = id;
        _leftBindingPriority = leftBindingPriority;
    }

    public Symbol(String id){
        this(id,0);
    }

    public String getID(){
        return _id;
    }

    public int getLeftBindingPriority() {
        return _leftBindingPriority;
    }

    public abstract ParseTreeEntry getNud(ParseTreeEntry self, ExpressionTreeBuilder builder) throws ExpressionTreeBuilderException, ParseException;
    public abstract ParseTreeEntry getLed(ParseTreeEntry self, ExpressionTreeBuilder builder,ParseTreeEntry left) throws ExpressionTreeBuilderException, ParseException;


}
