/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/


package com.thinkulator.parser;

/**
 *
 * @author hack
 */
public class ExpressionTreeBuilderException extends Exception{

    private Token token;
    
    public ExpressionTreeBuilderException(String msg) {
        super(msg);
    }
    
    public ExpressionTreeBuilderException(String msg, Token tok) {
        super(msg+" ["+tok.getValue()+"] at "+tok.getFrom()+" to "+tok.getTo());
        this.token = tok;
    }

    public ExpressionTreeBuilderException(String msg,Throwable t, Token tok) {
        super(msg+" ["+tok.getValue()+"] at "+tok.getFrom()+" to "+tok.getTo(),t);
        this.token = tok;
    }

    public ExpressionTreeBuilderException(Throwable t, Token tok) {
        super(t);
        this.token = tok;
    }

    public Token getToken(){
        return this.token;
    }

    public String toString(){
        if(token == null){
            return super.toString();
        }else{
            return super.toString()+" Token:"+token.toString();
        }
        
    }
}
