/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package com.thinkulator.dynamic;

import com.thinkulator.compiler.CompilationResult;
import com.thinkulator.compiler.ExpressionCLangCompiler;
import com.thinkulator.parser.ExpressionTokenizer;
import com.thinkulator.parser.ExpressionTreeBuilder;
import com.thinkulator.parser.ExpressionTreeBuilderException;
import com.thinkulator.parser.ParseTreeEntry;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author hack
 */
public class DynamicMain {
    public static void main(String [] args) throws IOException, ExpressionTreeBuilderException, ParseException{
        String calcFunc = args[0];
        
        
        ExpressionCLangCompiler instance = new ExpressionCLangCompiler();
//System.out.println(calcFunc);
        ExpressionTokenizer et = new ExpressionTokenizer(calcFunc);
        ExpressionTreeBuilder tb = ExpressionTreeBuilder.getBuilder(et);
        ParseTreeEntry pte = tb.parse();

        CompilationResult result = instance.compile(pte);
        
        
        File testWD = new java.io.File( "./binary_builder" );
        if(!testWD.exists()){
            System.err.println("Build directory does not exist: "+testWD.getAbsolutePath());
            System.exit(1);
        }
        //TODO: check that we have the right references
        //assertEquals("Number of references",1,result.getReferences().size());
        //assertEquals("Reference 0",result.getReferences().get(0),"X");
        //DEBUG, print the calculated script 
        File f = File.createTempFile("build_", ".c", testWD);
        //Create the program C source code.
        try (FileWriter fw = new FileWriter(f)) {
            fw.write("#include \"utils.h\"\n\n");
            fw.write("/*");
            fw.write(StringEscapeUtils.escapeJava(calcFunc.replace("*/", "*//*")));
            fw.write("*/\n");
            fw.write("char *  __attribute__ ((noinline)) __attribute__((section(\".text\"))) _start(unsigned long now,struct operations_ptr *ptr){\n");

            fw.write("\treturn ");
            fw.write(result.getCalculationSource());
            fw.write(";\n");

            fw.write("}\n");
            
            fw.write("\n");
            fw.write("block_header myHeader __attribute__((section(\".header\"))) = {\n" +
                "	.block_type = 0x01,\n" +
                "        .block_size = 0x00,\n" +
                "        .entry_offset = (uint32_t) &_start,\n" +
                "        .setup_offset = 0x00,\n" +
                "        .sha256_sig = 0x00\n" +
                "};");
        }
        
        String fileName = f.getName();
        String targetName = fileName.split("\\.")[0]+".hex";
        
        StringBuilder compileResult = new StringBuilder();
        {
            ProcessBuilder builder = new ProcessBuilder(
                "/usr/bin/make","-s", targetName);
            builder.directory(testWD);
            builder.redirectErrorStream(true);
            Process p = builder.start();
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while (true) {
                line = r.readLine();
                if (line == null) { break; }
                compileResult.append(line);
                System.out.println(line);
            }
        }
        System.out.println(testWD.toString()+"/"+targetName);
    }
}
