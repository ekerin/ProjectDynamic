/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package com.thinkulator.store;

import com.bootseg.orm.JNDI;
import com.bootseg.orm.ORM;
import com.bootseg.orm.ORMException;
import com.thinkulator.compiler.CompilationResult;
import com.thinkulator.compiler.ExpressionCLangCompiler;
import com.thinkulator.dynamic.beans.DeviceFormula;
import com.thinkulator.dynamic.beans.Formula;
import com.thinkulator.parser.ExpressionTokenizer;
import com.thinkulator.parser.ExpressionTreeBuilder;
import com.thinkulator.parser.ExpressionTreeBuilderException;
import com.thinkulator.parser.ParseTreeEntry;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.naming.NamingException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author hack
 */
public class FormulaStore {
    private static TreeMap<String,TreeMap<String,CompiledFormula>> store  = new TreeMap<>();
    
    
    
    public static CompiledFormula getDeviceCode(String deviceID,String formulaID){
        TreeMap<String,CompiledFormula> device = store.get(deviceID);
        if(device == null){
            return null;
        }
        return device.get(formulaID);
    }
    
    public static SortedMap<String,CompiledFormula> getAllDeviceCode(String deviceID){
        TreeMap<String,CompiledFormula> dev = store.get(deviceID);
        if(dev != null){
            return Collections.unmodifiableSortedMap(store.get(deviceID));
        }else{
            return new TreeMap<>();
        }
    }
    public static Set<String> getDevices(){
        return Collections.unmodifiableSet(store.keySet());
    }
    
    
    public static void setDeviceCode(String deviceID,String formulaID,CompiledFormula code){
        TreeMap<String,CompiledFormula> device = store.get(deviceID);
        if(device == null){
            device = new TreeMap<>();
            store.put(deviceID, device);
        }
        device.put(formulaID, code);
    }
    
    public static Formula getFormula(String formula) throws ExpressionTreeBuilderException, ParseException, IOException, SQLException, NamingException, ORMException{
        try(Connection conn = JNDI.getConnection("java:comp/env/jdbc/dynamic")){
            List<Formula> res = ORM.executeSelect(conn, Formula.class, "SELECT * FROM FORMULAS where formula_id = ?", DigestUtils.sha256Hex(formula));
            if(!res.isEmpty()){
                return res.get(0);
            }
        }
        
        ExpressionCLangCompiler instance = new ExpressionCLangCompiler();
        ExpressionTokenizer et = new ExpressionTokenizer(formula);
        ExpressionTreeBuilder tb = ExpressionTreeBuilder.getBuilder(et);
        ParseTreeEntry pte = tb.parse();

        CompilationResult result = instance.compile(pte);
        
        //TODO: Make this a config setting...
        //File testWD = new java.io.File( "/Users/hack/Thinkulator/ProjectDynamic/DynamicLib/binary_builder" );
        File testWD = new java.io.File( "/home/hack/Thinkulator/ProjectDynamic/DynamicLib/binary_builder" );
        if(!testWD.exists()){
            System.err.println("Build directory does not exist: "+testWD.getAbsolutePath());
            throw new RuntimeException("FAILED: Build directory does not exist: "+testWD.getAbsolutePath());
        }
        //TODO: check that we have the right references
        //assertEquals("Number of references",1,result.getReferences().size());
        //assertEquals("Reference 0",result.getReferences().get(0),"X");
        //DEBUG, print the calculated script 
        File f = File.createTempFile("build_", ".c", testWD);
        //Create the program C source code.
        try (FileWriter fw = new FileWriter(f)) {
            fw.write("#include <stdint.h>\n");
            fw.write("#include \"utils.h\"\n\n");
            fw.write("/*");
            fw.write(StringEscapeUtils.escapeJava(formula.replace("*/", "*//*")));
            fw.write("*/\n");
            fw.write("char *  __attribute__ ((noinline)) __attribute__((section(\".text\"))) _start(unsigned long now,struct operations_ptr *ptr){\n");

            fw.write("\treturn ");
            fw.write(result.getCalculationSource());
            fw.write(";\n");

            fw.write("}\n");
            
                        fw.write("\n");
            fw.write("block_header myHeader __attribute__((section(\".header\"))) = {\n" +
                "	.block_type = 0x01,\n" +
                "        .block_size = 0x00,\n" +
                "        .entry_offset = (uint32_t) &_start,\n" +
                "        .setup_offset = 0x00,\n" +
                "        .sha256_sig = 0x00\n" +
                "};");
        }
        
        String fileName = f.getName();
        String targetName = fileName.split("\\.")[0]+".hex";
        
        StringBuilder compileResult = new StringBuilder();
        {
            ProcessBuilder builder = new ProcessBuilder(
                "/usr/bin/make","-s", targetName);
            builder.directory(testWD);
            builder.redirectErrorStream(true);
            Process p = builder.start();
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while (true) {
                line = r.readLine();
                if (line == null) { break; }
                compileResult.append(line);
                compileResult.append("\n");
            }
        }
        
        String sha256sum = null;
        
        //did we get the sum injection header?
        int headerStart = compileResult.indexOf("---BEGIN HEADER INJECTION---");
        if(headerStart >= 0){
            headerStart = compileResult.indexOf("\n", headerStart); //go to the end of line
        }
        int headerEnd = compileResult.indexOf("---END HEADER INJECTION---");
        if(headerEnd != -1 && headerStart != -1){
            String headerInjectionJSON = compileResult.substring(headerStart, headerEnd);
            System.out.println(headerInjectionJSON);

            try (JsonReader rdr = Json.createReader(new StringReader(headerInjectionJSON))) {
                JsonObject headerInjection = rdr.readObject();
                System.out.println(headerInjection.getJsonObject("new"));
                sha256sum = headerInjection.getJsonObject("new").getString("SHA256_sig");
            }
            
        }else{
            System.out.println("Injection header not found in compilation output:");
            System.out.println(compileResult);
        }
    
        
        
        
        File res = new File(testWD,targetName);
        StringBuilder resSB;
        try (FileReader fr = new FileReader(res); BufferedReader resBR = new BufferedReader(fr)) {
            resSB = new StringBuilder(4096);
            while(true){
                String l = resBR.readLine();
                if(l == null){break;}
                resSB.append(l);
                resSB.append("\n");
            }          
        }
        
        Formula retval = new Formula(formula, 0x01,resSB.toString(), sha256sum);
        try(Connection conn = JNDI.getConnection("java:comp/env/jdbc/dynamic")){
            ORM.insertFromObject(conn, retval, "formulas");
        }
        return retval;
    }
    
    
    public static Formula setDeviceFormula(String deviceID,String ordinal,String formula) throws ExpressionTreeBuilderException, ParseException, IOException, SQLException, NamingException, ORMException{
        Formula f = getFormula(formula);
       

        setDeviceCode(deviceID,ordinal,new CompiledFormula(f.getHex(),f.getHexHash()));
        DeviceFormula df = new DeviceFormula(deviceID,f.getFormulaID(),ordinal);
        
        try(Connection conn = JNDI.getConnection("java:comp/env/jdbc/dynamic")){
            
            ORM.insertFromObject(conn, df, "device_formulas");
        }
        
        return f;
    }
    
    public static Formula getFormulaByBinHash(String hash) throws ParseException, IOException, SQLException, NamingException, ORMException{
        try(Connection conn = JNDI.getConnection("java:comp/env/jdbc/dynamic")){
            List<Formula> res = ORM.executeSelect(conn, Formula.class, "SELECT * FROM FORMULAS where binary_hash = ?", hash);
            if(!res.isEmpty()){
                return res.get(0);
            }else{
                throw new ORMException("Not Found");
            }
        }
        
    }
}
