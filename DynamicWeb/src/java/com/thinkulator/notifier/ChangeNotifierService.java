/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package com.thinkulator.notifier;

import com.thinkulator.store.CompiledFormula;
import com.thinkulator.store.FormulaStore;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author hack
 */
public class ChangeNotifierService {

    private final ConcurrentMap<String,ConcurrentLinkedQueue<DeviceChannelChange>> queue;
    private final Server srv;
    private final Thread srvThread;
    
    @SuppressWarnings("CallToThreadStartDuringObjectConstruction") //Seriously, who cares?
    public ChangeNotifierService(InetAddress hostAddress, int port) throws IOException {
        this.queue = new ConcurrentHashMap<>();
        this.srv = new Server(null, 5555, queue);
        
        this.srvThread = new Thread(this.srv);
        this.srvThread.setDaemon(true);
        this.srvThread.start();
    }

    public void notifyChange(String deviceID, String channelID, String hexHash){
        ConcurrentLinkedQueue<DeviceChannelChange> list;
        synchronized(queue){
            list = queue.get(deviceID);
            if(list == null){
                list = new ConcurrentLinkedQueue<>();
                queue.put(deviceID,list);
            }
        }
        list.add(new DeviceChannelChange(deviceID, channelID, hexHash));
    }

    void shutdown() throws InterruptedException, IOException{
        srv.shutdown();
        this.srvThread.join();
    }
    
    public Set<String> getSubscribedDevices(){
        return Collections.unmodifiableSet(this.srv.deviceKeyMapping.keySet());
    }
    
    private class DeviceChannelChange {

        private final String deviceID;
        private final String channelID;
        private final String newHashCode;

        public DeviceChannelChange(String deviceID, String channelID, String hashCode) {
            this.deviceID = deviceID;
            this.channelID = channelID;
            this.newHashCode = hashCode;
        }

        public String getHashCode() {
            return newHashCode;
        }

        /**
         * @return the deviceID
         */
        public String getDeviceID() {
            return deviceID;
        }

        /**
         * @return the channelID
         */
        public String getChannelID() {
            return channelID;
        }
    }

    private class Server implements Runnable {

        private final InetAddress hostAddress;
        private final int port;

        // The channel on which we'll accept connections
        private final ServerSocketChannel serverChannel;

        // The selector we'll be monitoring
        private final Selector selector;

        // The buffer into which we'll read data when it's available
        private final ByteBuffer readBuffer = ByteBuffer.allocate(8192);

        /** Stores the list of changes we need to send out notifications*/
        private final ConcurrentMap<String,ConcurrentLinkedQueue<DeviceChannelChange>> queue;
        
        /** Stores the mapping of device IDs to the selector key representing its connection.*/
        private final ConcurrentMap<String,SelectionKey> deviceKeyMapping; 
        /** Reverse of deviceKeyMapping */
        private final ConcurrentMap<SelectionKey,String> keyDeviceMapping; 
         
        private boolean keepRunning = true;
        public Server(InetAddress hostAddress, int port, ConcurrentMap<String,ConcurrentLinkedQueue<DeviceChannelChange>> queue) throws IOException {
            this.hostAddress = hostAddress;
            this.port = port;
            this.queue = queue;

            this.deviceKeyMapping = new ConcurrentHashMap<>();
            this.keyDeviceMapping = new ConcurrentHashMap<>();
            
            this.selector = SelectorProvider.provider().openSelector();

            // Create a new non-blocking server socket channel
            this.serverChannel = ServerSocketChannel.open();
            serverChannel.configureBlocking(false);

            // Bind the server socket to the specified address and port
            InetSocketAddress isa = new InetSocketAddress(this.hostAddress, this.port);
            serverChannel.socket().bind(isa);
                    
            System.out.println("Change Notifier listening on: "+serverChannel.getLocalAddress().toString());

            // Register the server socket channel, indicating an interest in 
            // accepting new connections
            serverChannel.register(this.selector, SelectionKey.OP_ACCEPT);
        }

        public void shutdown() throws IOException{
            this.keepRunning = false;
        }
        
        @Override
        public void run() {
            while (this.keepRunning) {
                try {
                    
                    Iterator<String> queueKeys = queue.keySet().iterator();
                    while(queueKeys.hasNext()){
                        String deviceID = queueKeys.next();
                        SelectionKey key = deviceKeyMapping.get(deviceID);
                        
                        if(key != null){
                            if(!key.channel().isOpen()){
                                continue; //sskip if the key has not yet been accepted.
                            }
                            
                            //TODO: This will keep happening till we send it.. Which is pretty wasteful of CPU.
                            //Figure out a better way to mark if we have already requested write for this SelectionKey
                            int ops = key.interestOps();
                            if((ops & SelectionKey.OP_WRITE) != SelectionKey.OP_WRITE){
                                int vops = key.channel().validOps();
                                key.interestOps(SelectionKey.OP_READ|SelectionKey.OP_WRITE);
                            }
                        }else{
                            //device ID is not connected, remove from queue now.
                            queueKeys.remove();
                        }
                        
                    }
                }catch(Exception e){
                      e.printStackTrace();
                }
                
                //Split so that the selection always happens, even if interest changes don't.
                try{
                    // Wait for an event one of the registered channels
                    this.selector.select(10);

                    // Iterate over the set of keys for which events are available
                    Iterator selectedKeys = this.selector.selectedKeys().iterator();
                    while (selectedKeys.hasNext()) {
                        SelectionKey key = (SelectionKey) selectedKeys.next();
                        selectedKeys.remove();

                        if (!key.isValid()) {
                            //clean up the mappings now that the connection is dead.
                            deviceKeyMapping.remove(keyDeviceMapping.get(key));
                            keyDeviceMapping.remove(key);
                            continue;
                        }
                        
                        // Check what event is available and deal with it
                        if (key.isAcceptable()) {
                            this.accept(key);
                        } else if (key.isReadable()) {
                            this.read(key);
                        } else if (key.isWritable()) {
                            this.write(key);
                        } 
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            
            
            try {
                this.serverChannel.close();

                //time to cleanup!
                for(SelectionKey key:this.selector.keys()){
                    key.channel().close();
                }
                this.selector.close();
                
                
            } catch (IOException ex) {
                Logger.getLogger(ChangeNotifierService.class.getName()).log(Level.SEVERE, null, ex);
            }

                
           
        }

        private void accept(SelectionKey key) throws IOException {
            // For an accept to be pending the channel must be a server socket channel.
            ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();

            // Accept the connection and make it non-blocking
            SocketChannel socketChannel = serverSocketChannel.accept();
            socketChannel.configureBlocking(false);
            socketChannel.setOption(StandardSocketOptions.TCP_NODELAY, true);
            
            // Register the new SocketChannel with our Selector, indicating
            // we'd like to be notified when there's data waiting to be read
            SelectionKey nKey = socketChannel.register(this.selector, SelectionKey.OP_READ);
            
        }
        
        private void read(SelectionKey key) throws IOException {
            SocketChannel socketChannel = (SocketChannel) key.channel();

            // Clear out our read buffer so it's ready for new data
            this.readBuffer.clear();

            // Attempt to read off the channel
            int numRead;
            try {
                numRead = socketChannel.read(this.readBuffer);
            } catch (IOException e) {
                // The remote forcibly closed the connection, cancel
                // the selection key and close the channel.
                key.cancel();
                socketChannel.close();
                return;
            }

            if (numRead == -1) {
                // Remote entity shut the socket down cleanly. Do the
                // same from our end and cancel the channel.
                key.channel().close();
                key.cancel();
                return;
            }

            //do nothing with the data for now.
            this.readBuffer.flip();
            System.out.println("Received: "+new String(this.readBuffer.array(), this.readBuffer.arrayOffset(), this.readBuffer.remaining()));
            
            String message = new String(this.readBuffer.array(), this.readBuffer.arrayOffset(), this.readBuffer.remaining());
            try (JsonReader rdr = Json.createReader(new StringReader(message))) {
                JsonObject jsonMessage = rdr.readObject();
                
                if(jsonMessage.containsKey("address")){
                    String macAddress = jsonMessage.getString("address");
                    System.out.println("Subscribing to MAC: "+macAddress);
                    deviceKeyMapping.put(macAddress, key);
                    keyDeviceMapping.put(key,macAddress);
                    
                    //send notifications for any existing formulas
                    SortedMap<String,CompiledFormula> currentFormulas = FormulaStore.getAllDeviceCode(macAddress);
                    
                    for(String outID:currentFormulas.keySet() ){
                        notifyChange(macAddress,outID,currentFormulas.get(outID).getSha256sum());
                    }
                }
            }
        }

        
        private void write(SelectionKey key) throws IOException {
            SocketChannel socketChannel = (SocketChannel) key.channel();

            ConcurrentLinkedQueue<DeviceChannelChange> list;
            synchronized(queue){
                list = queue.get(keyDeviceMapping.get(key));
                if(list == null){
                    //WTF...
                    throw new RuntimeException("List missing for scheduled write operation for device ID: "+keyDeviceMapping.get(key));
                }
            }
            DeviceChannelChange change;
            while((change = list.poll()) != null){
                //JsonObject obj = Json.createObjectBuilder().add("channel", change.getChannelID()).add("hash", change.newHashCode).build();
                
                ByteBuffer buf = ByteBuffer.wrap((change.getChannelID()+","+change.getHashCode()+"\n").getBytes());
                //buf.flip();
                System.out.println("Writing: "+new String(buf.array(), buf.arrayOffset(), buf.remaining()));
                socketChannel.write(buf);
                
                if(buf.remaining() > 0){
                    //TODO: Handle this...
                    throw new RuntimeException("Unhandled incomplete Write operation sending data to deviceID: "+change.getDeviceID()+" for channel: "+change.getChannelID());
                }
            }
            
            //finally, turn off write selector interest (if one got added later, it will get turned back on.
            key.interestOps(SelectionKey.OP_READ);
	}
    }
}
