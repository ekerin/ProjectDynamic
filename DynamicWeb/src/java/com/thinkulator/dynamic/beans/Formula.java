/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package com.thinkulator.dynamic.beans;

import com.bootseg.orm.Column;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author hack
 */
public class Formula {
    @Column(value = "formula_id",isPrimary = true)   
                            private String _formulaID;
    @Column("formula")      private String _formula;
    @Column("type")         private Integer _type;
    @Column("hex")          private String _hex;
    @Column("binary_hash")  private String _hexHash; //not the hash of the hex itself, but the signature of the executable code section of the binary contained within.
    
    protected Formula(){
        
    }
    
    public Formula(String formula,int type,String hex,String hexHash){
        _formulaID = DigestUtils.sha256Hex(formula);
        _formula = formula;
        _type = type;
        _hex = hex;
        _hexHash = hexHash; //TODO: calculate this instead of passing it
    }

    /**
     * @return the _formulaID
     */
    public String getFormulaID() {
        return _formulaID;
    }

    /**
     * @return the _formula
     */
    public String getFormula() {
        return _formula;
    }

    /**
     * @return the _type
     */
    public Integer getType() {
        return _type;
    }

    /**
     * @return the _hex
     */
    public String getHex() {
        return _hex;
    }

    /**
     * @return the _hexHash
     */
    public String getHexHash() {
        return _hexHash;
    }
    
    
}
