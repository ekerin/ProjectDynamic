/** Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package com.thinkulator.dynamic.beans;

import com.bootseg.orm.Column;
import com.bootseg.orm.Table;

/**
 *
 * @author hack
 */
@Table("device_formula")
public class DeviceFormula {
    @Column(value = "device_id",isPrimary = true)   private String _deviceID;
    @Column(value = "ordinal",isPrimary = true)  private String _ordinal;
    @Column(value = "formula_id")  private String _formulaID;
    

    public DeviceFormula() {
    }

    public DeviceFormula(String _deviceID, String _formulaID, String _ordinal) {
        this._deviceID = _deviceID;
        this._formulaID = _formulaID;
        this._ordinal = _ordinal;
    }

    public String getDeviceID() {
        return _deviceID;
    }

    public String getFormulaID() {
        return _formulaID;
    }

    public String getOrdinal() {
        return _ordinal;
    }
}
