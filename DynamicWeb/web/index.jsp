<%-- Copyright 2016-2017 Thinkulator, LLC

  Project Dynamic is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

--%><%@page import="com.thinkulator.notifier.ChangeNotifierServletListener"%>
<%@page import="java.util.Set"%><%@page import="com.thinkulator.store.FormulaStore"%><%
%><%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%><%@page contentType="text/html" pageEncoding="UTF-8"%><%
%><%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%
%><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Thinkulator Dynamic</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        
        <style type="text/css">
            .syncing {
                background-color:yellow;
            }
            
            .good {
                background-color:lightgreen;
            }
            
            .bad {
                background-color:red;
            }
        </style>
            
    </head>
    
    <body>
<%
    request.setAttribute("devices", ChangeNotifierServletListener.getNotiferService().getSubscribedDevices());
%>
    <c:forEach var="device" items="${devices}">
    <h2><c:out value="${device}"/></h2>
        <c:forEach var="i" begin="0" end="15" step="1">
        <sql:query var="formula" dataSource="jdbc/dynamic" sql="select formula from formulas inner join device_formulas on formulas.formula_id = device_formulas.formula_id where device_id = ? and ordinal = ?"><sql:param value="${device}"/><sql:param value="${i}"/></sql:query>
        
            <form name="<c:out value="${device}"/>_c<c:out value="${i}"/>" action="<c:url value="/SetFormula"/>" method="POST" target="save_formula">
            <input name="device" value="<c:out value="${device}"/>" type="hidden">
            <input name="channel" value="<c:out value="${i}"/>" type="hidden">
            Output <c:out value="${i}"/>: <input name="formula" size="50" value="<c:out value="${formula.rows[0].formula}"/>" type="text" onchange="updateFormula(this.form)">
            <input type="button" value="Set Formula" onClick="updateFormula(this.form)"> 
        </form>
        </c:forEach>
    </c:forEach>

    <script type="text/javascript">
        function updateFormula(frm){
            $(frm).addClass("syncing").removeClass("bad").removeClass("good");
            $.ajax({
                url: frm.getAttribute("action")+"?"+$(frm).serialize()
            }).done(function(data){
                $(frm).removeClass("syncing").removeClass("bad").addClass("good");
            }).fail(function(data){
                $(frm).removeClass("syncing").addClass("bad").removeClass("good");
            });
        }
    </script>
        
    
    </body>

</html>
